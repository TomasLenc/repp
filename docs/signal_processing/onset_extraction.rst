=================
Onset extraction
=================

The first part of the signal processing concerns the onset extraction procedure. In particular,
REPP takes the mono recording and outputs the estimated onsets of the markers and tapping response following the next steps:

1. Channel separation

2. Cleaning procedure: markers and tapping signal

3. Onset extraction


1. Channel separation
-----------------------

Given the mono recording, we generate three channels for each of the components we will use in subsequent steps:
``rec_markers``, ``rec_test``, and `rec_tapping``, using the filtering procedure described in ``stimulus_preparation``.

.. autofunction:: repp.signal_processing.extract_audio_signals

The same can be applied to the tapping response only (e.g., in unconstrained tapping paradigms where markers are no needed):

.. autofunction:: repp.signal_processing.extract_audio_signals_tapping_only


To extract the envelope of the sound, we perform two operations on each channel. First, we max normalize each channel,
then we perform envelope extraction by performing max of the amplitude in a window of 2 ms and a nonlinear compression
by exponentiating the absolute value of the envelope with factor of 1.3, a standard preparation step
in signal processing (see McDermott &  Simoncelli, 2011):

.. autofunction:: repp.signal_processing.channel_separation

.. autofunction:: repp.signal_processing.channel_separation_only_tapping


2. Cleaning procedure
-----------------------

Here we use a signal cleaning heuristic to improve the signal obtained from ``rec_markers`` in the previous signal processing step.
Although this heuristic is not essential, it will help to increase the robustness of the process with noisy recordings or with strong
noise cancellation technologies. This process returns an enhanced signal for the markers (``rec_markers_clean``).


.. autofunction:: repp.signal_processing.signal_cleaning

The cleaning procedure for the tapping response is different because of the nature of signal and potential artifacts.
In particular, since several steps in the signal processing pipeline rely on max normalization, the tapping signal is
sensitive to both noise artifacts and cases where participants do not tap at all. To account for this, we attenuate significantly
(but not ignore) the audio level in the beginning and end of the recording, attenuating by a factor of ``clean_normalize_factor``
the first ⅓ and last ⅓ of the recording. This process returns an enhanced signal for the tapping response (``rec_tapping_clean``).


.. autofunction:: repp.signal_processing.signal_cleaning_tapping


3. Onset extraction
-----------------------

This step applies a simple onset extraction algorithm to detect all samples exceeding a relative threshold (``threshold``).
The extraction collected all supra-threshold samples ignoring any repeated threshold crossing separated by less than a given time window
(``first_window``), a measure used to avoid the detection of signal from the same onset. We further  discarded supra-threshold samples that
were not separated from the one preceding them by at least a second time window (``second_window``), a measure used to avoid the detection of
signal coming from repeated taps or noise, such as when hitting the tapping board with two fingers or the vibration produced by moving parts on the board).
We apply this process to ``rec_markers_clean`` and ``rec_tapping_clean``, returning a list of raw extracted tapping onsets and extracted marker onsets.


.. autofunction:: repp.signal_processing.detect_onsets

.. autofunction:: repp.signal_processing.extract_onsets

The same can be applied to the tapping response only (e.g., in unconstrained tapping paradigms where markers are no needed):

.. autofunction:: repp.signal_processing.extract_onsets_only_tapping

We also prepare the initial stimulus onsets to be ready for the next step:

.. autofunction:: repp.signal_processing.prepare_onsets_extraction
