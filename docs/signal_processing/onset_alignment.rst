=================
Onset alignment
=================

We next align the list of shifted stimulus onsets (``shifted_stimulus_onsets``) with the list of extracted tapping onsets (``tapping_detected_onsets``).
In this step, we always use the first marker onset in ``markers_detected_onsets`` as the single frame of reference.
The other markers can be used to estimate the markers’ detection error (see `Performance Analysis`). The output of this process is the aligned list of
stimulus onsets (``stim_onsets_aligned``) and the aligned list of tapping onsets (``resp_onsets_aligned``), where there is one onset for each stimulus
onset (or a NA to indicate a missing response).

Note that the ''onset_matching_window'' should be selected based on the requirements of each individual experiment and the desired ISI.
We point to the extensive body of research on this topic to inform such decisions (see Repp, 2005; Repp & Su, 2013). For example, it is common to use
fixed matching windows of 200-250 ms, as asynchronies are typically smaller than 150 ms (Repp 2005). However, it is worth noting that this selection only
affects edge cases: for most participants and tapping trials, the asynchrony will be smaller and therefore unaffected by the size of onset_matching_window.
Another aspect considered in the onset alignment process is the fact that participants tend to tap with a negative and fixed mean asynchrony (Repp 2005).
Because of this, we define a matching window symmetrically around the perceptual centre (``mean_asynchrony``).



.. autofunction:: repp.signal_processing.align_onsets


.. autofunction:: repp.signal_processing.compute_matched_onsets


.. autofunction:: repp.signal_processing.mean_asynchrony


.. autofunction:: repp.signal_processing.raw_onsets_to_matched_onsets
