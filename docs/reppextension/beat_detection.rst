
============
Beat finding
============

Contributor: **Vani Rajendran**

This extension supports beat-finding tapping experiments in response to a REPEATING
(or approximately repeating) stimulus. We demonstrate this code in the demo ``beatfinding_cyclic.ipynb``, which
includes two examples: (a) tapping to non-isochronous repeating rhythm and (b) beat synchronization to a
repeating sound clip like the speech-to-song illusion. Both examples load files from the input dir,
but see the ``sms_tapping.ipynb`` demo for an example of generating stimuli on the fly.

Depending on the task, you will need to update some params in ``config.py``. You need to provide a stimulus.wav and
stimulus.txt file. The .txt file should contain virtual "onsets" that define your stimulus, which may be
"played" or not. See the example .txt files to understand how to define the onsets a repeating rhythmic
pattern or something less defined like speech. ``N_CYCLES`` should be set to how many times a cycle repeats
in each trial. ``ID_OF_CYCLE_START`` corresponds to the index in (your stimulus.txt file) that corresponds
the start of a cycle. ``CYCLE_DUR_S`` is the duration of one cycle in seconds.


The main chunk of new code is in the wrapper function to analyze and visualize the recording
(``do_beat_detection_analysis``). The extention comes with plotting functionalities for beat finding tasks.
Set ``PLOT_TYPE`` depending on whether you want to see tapping aligned
to either an ONSETS, WAFEFORM, or SPECTROGRAM representation of your stimulus.

.. image:: ../_static/images/beatfinding_plot.jpg
  :alt: steps


See ``beatfinding_cyclic`` demo for a demonstration.
