=====
About
=====

REPP (Rhythm ExPeriment Platform) is a novel technology for measuring SMS in online experiments that can
work efficiently using the built-in microphone and speakers of standard laptop computers. The audio stimulus
(e.g., a metronome or a music excerpt) is played through the speakers and the resulting signal is recorded along
with participants’ responses in a single channel. The resulting recording is then analyzed using signal processing
techniques to extract and align timing cues with high temporal accuracy. REPP is fully automated and customizable,
enabling researchers to monitor experiments in real time and to implement a wide variety of SMS paradigms.

This technology is named after Dr. Bruno Repp, notable for his work on sensorimotor
synchronization, rhythm, and timing perception. His pioneering research has been truly inspirational.

See `publication in Behavior Research Methods <https://doi.org/10.3758/s13428-021-01722-2>`_ for a detailed description and validation of REPP.

The source code is hosted at `this Gitlab repo <https://gitlab.com/computational.audition/repp>`_.


Development
-----------

`REPP` was developed at the
`Max Planck Institute for Empirical Aesthetics <https://www.aesthetics.mpg.de/en>`_
by Manuel Anglada-Tort, Peter Harrison, and Nori Jacoby, with help and feedback from other members of the
`Computational Auditory Perception research group
<https://www.aesthetics.mpg.de/en/research/research-group-computational-auditory-perception.html>`_.


Contributors
-------------

**Vani Rajendran**: developed analysis and plotting methods for beat detection tasks
(see ``beat_detection`` in ``reppextension`` and ``beatfinding_cyclic`` demo)

Contributing
-------------
We welcome contributions supporting new paradigms or improvements in the current code.
Please talk to one of the authors if you would like to contribute.


Feedback
--------

Any feedback about the package is of course welcome,
just get in touch with one of the authors.
