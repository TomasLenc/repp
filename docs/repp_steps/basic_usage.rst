=================
Basic usage
=================

Before starting an experiment, the list of global parameters have to be imported (see ``config``).
Each experimental paradigm should have its own configuration object containing all global parameters.
We created a list of optimised parameters that,
based on our experience, work efficiently to perform non adaptive tapping experiments.

::

    from repp.config import sms_tapping
    config = sms_tapping


`REPP` is organised around five main steps:


1. Stimulus preparation

2. Recording phase

3. Onset extraction

4. Onset alignment

5. Performance analysis


.. image:: ../_static/images/steps.jpeg
  :alt: steps


The main methods supporting these steps are provided in the following modules (see ``demos``):

- ``stimulus``: functions supporting the stimulus preparation step, with the class :class:`~repp.stimulus.REPPStimulus`

- ``signal_processing``: functions supporting onset alignment and onset extraction

- ``analysis``: functions supporting the performance analysis, with the class :class:`~repp.analysis.REPPAnalysis`
