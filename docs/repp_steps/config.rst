===========
Config
===========

REPP needs a list of global parameters to determine different aspects of the stimulus preparation, signal processing
and analysis steps. The class :class:`~repp.config.ConfigUpdater` provides methods to help with this process.


1. Create a configuration class by specifying a dictionary with parameters: 

::

    sms_tapping = ConfigUpdater({
        # Stimulus preparation step
        'STIM_RANGE': [30,1000],
        'STIM_AMPLITUDE': 0.12,
        'MARKERS_RANGE': [200, 340],
        'TEST_RANGE': [100, 170],
        'MARKERS_AMPLITUDE': 0.9,
        'MARKERS_IOI': [0, 280, 230]
        })


2. Create a configuration object by updating an old one, either adding new parameters or modifying existing ones:

::

    new_config = ConfigUpdater.create_config(
        sms_tapping,
        {'LABEL': 'new config',
        'USE_CLICK_FILENAME': True,
        })


Parameters can then be called using the different attributes of the class, for example:

::

    fs = new_config.FS # sampling frequency
    markers_range = new_config.MARKERS_RANGE # Markers frequency range in Hz


See :class:`~repp.config.ConfigUpdater` with all global parameters:

.. automodule:: repp.config
    :show-inheritance:
    :members:
