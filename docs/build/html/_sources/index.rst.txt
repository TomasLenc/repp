REPP Documentation
====================

.. toctree::
   :maxdepth: 1
   :caption: Introduction

   introduction/about
   introduction/installation
   introduction/demo

.. toctree::
   :maxdepth: 2
   :caption: REPP documentation
   :glob:

   repp_steps/basic_usage
   repp_steps/config
   repp_steps/stimulus
   repp_steps/recording
   repp_steps/analysis

.. toctree::
   :maxdepth: 3
   :caption: Signal processing
   :glob:

   signal_processing/onset_extraction
   signal_processing/onset_alignment

.. toctree::
   :maxdepth: 4
   :caption: REPP extension
   :glob:

   reppextension/beat_detection
