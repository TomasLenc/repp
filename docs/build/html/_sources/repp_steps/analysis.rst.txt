===========
Analysis
===========

After the recording phase, the resulting recording can be analysed using REPP's signal processing pipeline.
:class:`~repp.analysis.REPPAnalysis` provides the wrapper methods for the analysis, including the signal processing
steps, performance analysis, and failing criteria (all functions supporting the onset extraction and onset
alignment procedures are in the ``signal_processing`` module).

`REPP` supports analysis for sensorimotor synchronization tasks (see ``sms_tapping`` demo):

::

    from repp.analysis import REPPAnalysis
    analysis = REPPAnalysis(config = non_adaptive_tapping) # specify the parameters configuration
    output, analysis, is_failed = analysis.do_analysis(
        stim_info, # dictionary with stimulus information (from the stimulus preparation step)
        recording_filename), # path to the recording file
        title_plot, # title figure
        plot_filename) # path to save the figure
    )


Or unconstrained tapping tasks without synchronization to an audio stimulus (see ``unconstrained_tapping`` demo):

::

    from repp.analysis import REPPAnalysis
    analysis = REPPAnalysis(config = non_adaptive_tapping) # specify the parameters configuration
    output, analysis, is_failed = analysis.do_analysis_tapping_only(
        stim_info, # dictionary with stimulus information (from the stimulus preparation step)
        recording_filename), # path to the recording file
        title_plot, # title figure
        plot_filename) # path to save the figure
    )


When running REPP in online settings, it is crucial to determine whether participants are tapping in the required way (e.g., following the instructions)
and whether there are any technical constraints that may preclude the recording of their signal, such as cases where the microphone is malfunctioning or the laptop
uses strong noise cancellation technology. To identify and exclude these cases, we suggest two failing criteria. First, since our technology will not work efficiently
unless it detects the marker sounds with high precision, we suggest failing trials in which not all markers are detected, or where the markers are displaced relative
to each other for a certain time window (e.g., 5 to 15 ms), using the number of detected markers onsets and the time error between the detected markers onsets and their
known locations, respectively. Second, we suggest failing trials where the response to stimulus ratio (``ratio_resp_to_stim``) is less than 50% or more than 200%.
This measure can be used to deter participants from not responding at all or from tapping at an extremely fast rate, irrespective of the audio stimuli.
Importantly, none of these criteria exclude trials based on actual tapping performance, but only based on whether the signal can be correctly recorded by
the technology and whether participants produced a minimally acceptable number of tapping responses. The results of the failing criteria are stored in ``is_failed``.

.. automodule:: repp.analysis
    :show-inheritance:
    :members:
