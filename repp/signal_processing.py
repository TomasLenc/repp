####################################################################################################
# File:     onset_extraction.py
# Purpose:  Main functions for onset extraction step
#
# Author:   Manuel Anglada-Tort, Peter Harrison, Nori Jacoby
####################################################################################################
import numpy as np

from .utils import (
    read_audio_file,
    simple_resample,
    band_pass_sharp,
    cheap_hilbert,
    fade_in_out,
    ms_to_samples
)


########################################
# Functions supporting onset extraction
########################################


def extract_onsets(audio_signals, config):
    """
    Extract onsets from audio signals

    :type audio_signals: dict
    :param audio_signals: Information from the extracted signals, including markers, test, and tapping response.

    :type config: class
    :param config: Configuration parameters for the experiment (see config.py).

    :return: Returns the raw extracted onsets for tapping and markers (dict).
    """
    fs0 = config.FS0
    threshold = config.EXTRACT_THRESH
    first_window = config.EXTRACT_FIRST_WINDOW
    second_window = config.EXTRACT_SECOND_WINDOW
    raw_onsets_extracted = {
        'markers_detected_onsets': detect_onsets(audio_signals['rec_markers_clean'], threshold[0], first_window[0],
                                                 second_window[0], fs0),
        'tapping_detected_onsets': detect_onsets(audio_signals['rec_tapping_clean'], threshold[1], first_window[1],
                                                 second_window[1], fs0)
    }
    return raw_onsets_extracted


def extract_onsets_only_tapping(audio_signals, config):
    """
    Extract onsets from audio signals

    :type audio_signals: dict
    :param audio_signals: Information from the signals extracted from the raw recording.

    :type config: class
    :param config: Configuration parameters for the experiment (see config.py).

    :return: Returns the raw extracted onsets for tapping (dict).
    """
    fs0 = config.FS0
    threshold = config.EXTRACT_THRESH
    first_window = config.EXTRACT_FIRST_WINDOW
    second_window = config.EXTRACT_SECOND_WINDOW
    tapping_detected_onsets = detect_onsets(audio_signals['rec_tapping_clean'], threshold[1], first_window[1],
                                            second_window[1], fs0)

    raw_onsets_extracted = {
        'tapping_detected_onsets': tapping_detected_onsets,
        'num_tapping_detected_onsets': len(tapping_detected_onsets)
    }
    return raw_onsets_extracted


# detect onsets in a chunk
def detect_onsets(samples, threshold, first_window, second_window, fs):
    """
    Onset detection algorithm based on relative threshold

    :type samples: numpy.ndarray
    :param samples: List of audio samples to find onsets.

    :type threshold: list
    :param threshold: Relative threshold for the onset extraction procedure [tapping, markers].

    :type first_window: list
    :param first_window: First window to separate samples in the onset extraction procedure [tapping, markers] (ms).

    :type second_window: list
    :param second_window: Second window to separate samples in the onset extraction procedure [tapping, markers] (ms).

    :type fs: int
    :param fs: Sampling frequency.

    :return: Returns the raw extracted onsets  of the given samples (numpy.ndarray).
    """
    chunk = np.abs(
        samples)  # The first onset is the first location in samples, and it is used to perform the chunkwise
    # normalization with damping
    onsets = []
    last = 0
    for j, sample in enumerate(chunk):  # Apply sleeping threshold detector
        current = j
        if abs(sample) > threshold and (current - last) > ms_to_samples(
                first_window, fs):
            if (current - last) > ms_to_samples(second_window, fs):
                onsets.append(current)
            last = current
    filtered_onsets = filter_too_close_onsets(onsets, second_window, fs)
    detected_onsets = (1000.0 * np.array(filtered_onsets)) / fs
    return detected_onsets


def filter_too_close_onsets(onsets, second_window, fs):
    onsets = np.array(onsets)  # filter "too close" onset
    deltas = np.diff(onsets)
    shifted_deltas = np.concatenate([[np.inf], deltas])
    ok = shifted_deltas > ms_to_samples(second_window, fs)
    if len(onsets) != 0:
        filtered_onsets = onsets[ok]
    else:
        filtered_onsets = np.array([])
    return filtered_onsets


def extract_audio_signals(rec_filename, config):
    """
    Separate channels from mono recording and clean signals

    :type rec_filename: str
    :param rec_filename: Name of the audio recording.

    :type config: class
    :param config: Configuration parameters for the experiment (see config.py).

    :return: Returns the separated channels and relevant info for each element (dict).
    """
    samples, fs_rec, rec_downsampled, time_line_for_sample = downsample_audio(rec_filename, config.FS0)
    channels = channel_separation(
        rec_downsampled,
        config.FS0,
        config.EXTRACT_COMPRESS_FACTOR,
        config.EXTRACT_FADE_IN,
        config.TAPPING_RANGE,
        config.TEST_RANGE,
        config.MARKERS_RANGE)
    audio_signals = {
        'fs': config.FS0,
        'rec_original': samples,
        'fs_recording': fs_rec,
        'time_line_for_sample': time_line_for_sample,
        'rec_downsampled': rec_downsampled,
        **channels,
        **signal_cleaning(
            channels["rec_tapping"],
            channels["rec_test"],
            channels["rec_markers"],
            config.FS0,
            config.CLEAN_BIN_WINDOW,
            config.CLEAN_MAX_RATIO,
            config.CLEAN_LOCATION_RATIO,
            config.CLEAN_NORMALIZE_FACTOR
        )
    }
    return audio_signals


def downsample_audio(rec_filename, fs0):
    samples, fs_rec = read_audio_file(rec_filename)
    rec_downsampled = simple_resample(samples, fs_rec, fs0)
    # define time-line (simply the time for each sample used for plotting)
    time_line_for_sample = np.linspace(0, (len(rec_downsampled)) / (1.0 * fs0), len(rec_downsampled))
    rec_downsampled = rec_downsampled / np.max(rec_downsampled)  # max normalization
    return samples, fs_rec, rec_downsampled, time_line_for_sample


def extract_audio_signals_tapping_only(rec_filename, config):
    """
    Separate tapping channel from mono recording and clean signals

    :type rec_filename: str
    :param rec_filename: Name of the audio recording.

    :type config: class
    :param config: Configuration parameters for the experiment (see config.py).

    :return: Returns the seperated tapping channel with relevant info (dict).
    """
    samples, fs_rec, rec_downsampled, time_line_for_sample = downsample_audio(rec_filename, config.FS0)
    rec_tapping = channel_separation_only_tapping(
        rec_downsampled,
        config.FS0,
        config.EXTRACT_COMPRESS_FACTOR,
        config.EXTRACT_FADE_IN,
        config.TAPPING_RANGE)
    rec_tapping_clean, _, _ = signal_cleaning_tapping(
        rec_tapping,
        config.CLEAN_LOCATION_RATIO,
        config.CLEAN_NORMALIZE_FACTOR)
    audio_signals = {
        'fs': config.FS0,
        'rec_original': samples,
        'fs_recording': fs_rec,
        'time_line_for_sample': time_line_for_sample,
        'rec_downsampled': rec_downsampled,
        'rec_tapping': rec_tapping,
        'rec_tapping_clean': rec_tapping_clean
    }
    return audio_signals


def channel_separation_only_tapping(recording, fs, extract_compress_factor, fade_in, tapping_range):
    """
    Separate tapping channel from mono recording and clean signal

    :type recording: numpy.ndarray
    :param recording: Audio recording

    :type fs: int
    :param fs: Sampling frequency.

    :type extract_compress_factor: float
    :param extract_compress_factor: Compression exponent to extract the envelope from the recording.

    :type fade_in: float
    :param fade_in: Initial fade in to extract the envelope from the recording (ms).

    :type tapping_range: list
    :param tapping_range: Tapping frequency range (Hz): [min, max].

    :return: Returns the separated tapping channel.
    """
    if len(tapping_range) > 2:
        rec_tap = recording * 0
        for i in range(0, len(tapping_range), 2):
            temp = band_pass_sharp(recording, fs, tapping_range[i], tapping_range[i + 1])
            rec_tap = rec_tap + temp
    else:
        rec_tap = band_pass_sharp(recording, fs, min(tapping_range), max(tapping_range))
    rec_tap_normalised = 0.5 * rec_tap / max(rec_tap)  # normalize
    tapping_envelope = np.power(cheap_hilbert(rec_tap_normalised, fs),
                                extract_compress_factor)  # extract envelope and perform compression
    rec_tapping = fade_in_out(tapping_envelope, fs, fade_in)
    rec_tapping = 1.0 * rec_tapping / max(rec_tapping)
    return rec_tapping


def channel_separation(recording, fs, extract_compress_factor, fade_in, tapping_range, test_range, markers_range):
    """
    Separate channels from mono recording and clean signals

    :type recording: numpy.ndarray
    :param recording: Audio recording

    :type fs: int
    :param fs: Sampling frequency.

    :type extract_compress_factor: float
    :param extract_compress_factor: Compression exponent to extract the envelope from the recording.

    :type fade_in: float
    :param fade_in: Initial fade in to extract the envelope from the recording (ms).

    :type tapping_range: list
    :param tapping_range: Tapping frequency range (Hz): [min, max].

    :type test_range: list
    :param test_range: Test frequency range (Hz): [min, max].

    :type markers_range: int
    :param markers_range: Markers frequency range (Hz): [min, max].

    :return: Returns a dict with the three seperated channels: tapping, test, and markers.
    """
    # filter mono recording based on freq ranges
    tapping_channel = filter_recording(recording, tapping_range, fs)
    test_channel = filter_recording(recording, test_range, fs)
    markers_channel = filter_recording(recording, markers_range, fs)
    # normalize
    tapping_channel_normalized = 0.5 * tapping_channel / max(tapping_channel)
    # Both test and markers channels are normalized according to markers amplitude
    test_channel_normalized = 0.5 * test_channel / max(markers_channel)
    markers_channel_normalized = 0.5 * markers_channel / max(markers_channel)
    # extract envelope and perform compression:
    tapping_envelope = np.power(cheap_hilbert(tapping_channel_normalized, fs), extract_compress_factor)
    test_envelope = np.power(cheap_hilbert(test_channel_normalized, fs), extract_compress_factor)
    markers_envelope = np.power(cheap_hilbert(markers_channel_normalized, fs), extract_compress_factor)
    tapping_envelope = fade_in_out(tapping_envelope, fs, fade_in)
    test_envelope = fade_in_out(test_envelope, fs, fade_in)
    markers_envelope = fade_in_out(markers_envelope, fs, fade_in)
    channels = {
        "rec_tapping": 1.0 * tapping_envelope / max(tapping_envelope),
        "rec_test": 1.0 * test_envelope / max(markers_envelope),
        "rec_markers": 1.0 * markers_envelope / max(markers_envelope)
    }
    return channels


def filter_recording(recording, freq_range, fs):
    if len(freq_range) > 2:
        rec_filtered = recording * 0
        for i in range(0, len(freq_range), 2):
            temp = band_pass_sharp(recording, fs, freq_range[i], freq_range[i + 1])
            rec_filtered = rec_filtered + temp
    else:
        rec_filtered = band_pass_sharp(recording, fs, min(freq_range), max(freq_range))
    return rec_filtered


def signal_cleaning(rec_tapping, rec_test, rec_markers, fs, clean_bin_window, clean_max_ratio, clean_location_ratio,
                    clean_normalize_factor):
    """
    Cleaning heuristic to apply to markers and tapping channel

    :type rec_tapping: numpy.ndarray
    :param rec_tapping: Audio recording

    :type rec_test: int
    :param rec_test: Sampling frequency.

    :type rec_markers: float
    :param rec_markers: Compression exponent to extract the envelope from the recording.

    :type fs: int
    :param fs: Sampling frequency.

    :type clean_bin_window: float
    :param clean_bin_window: Bin width window to apply to markers' cleaning procedure (ms).

    :type clean_max_ratio: float
    :param clean_max_ratio: Maximum clean ratio allowed in the markers' cleaning procedure.

    :type clean_location_ratio: int
    :param clean_location_ratio: Ratio of the location to normalize the tapping signal in the cleaning procedure.

    :type clean_normalize_factor: int
    :param clean_normalize_factor:  Factor to normalize the tapping signal in the location ration.

    :return: Returns a dict with information from the cleaning procedure and the cleaned signals.
    """
    # clean markers from tapping by enhancing places were markers and test are different
    # (thereby eliminating the tapping signal that is recorded in both channels)
    rec_test_max = cheap_hilbert(rec_test, fs, window_length=clean_bin_window)
    rec_markers_max = cheap_hilbert(rec_markers, fs, window_length=clean_bin_window)
    # this ratio is large exactly where there is more energy in the markers compared to the test range
    ratio_clean_seed = rec_markers_max / (rec_test_max + 1e-10)
    ratio_clean = ratio_clean_seed.copy()
    ratio_clean = np.minimum(np.maximum(ratio_clean, np.ones(ratio_clean.size) * (1.0 / clean_max_ratio)),
                             np.ones(ratio_clean.size) * clean_max_ratio)
    rec_markers_clean = rec_markers * ratio_clean
    # Both test and markers channels are normalized according to markers amplitude
    rec_test_final = 1.0 * rec_test / max(rec_markers_clean)
    rec_markers_final = 1.0 * rec_markers / max(rec_markers_clean)
    rec_markers_clean = 1.0 * rec_markers_clean / max(rec_markers_clean)
    # clean tapping using different procedure
    rec_tapping_clean, start_include, end_include = signal_cleaning_tapping(rec_tapping, clean_location_ratio,
                                                                            clean_normalize_factor)
    # the ratio in max amp between the segment in the start / end compared with
    # the middle (should be small for test sequence)
    mid2begratio_markers = max(rec_markers_clean[start_include:end_include]) / max(
        max(rec_markers_clean[0:start_include]), max(rec_markers_clean[end_include:-1]))
    mid2begratio_tapping = max(rec_tapping_clean[start_include:end_include]) / max(
        max(rec_tapping_clean[0:start_include]), max(rec_tapping_clean[end_include:-1]))

    amplitude_markers = np.sqrt(np.mean(rec_markers ** 2))
    amplitude_test = np.sqrt(np.mean(rec_test ** 2))
    snr = amplitude_markers / amplitude_test  # compute SNR of test channel and real stimulus
    snr_info = {
        "snr": snr,
        "max_ratio": max(ratio_clean_seed),
        "mean_ratio": np.mean(ratio_clean_seed),
        "mid2begratio_markers": mid2begratio_markers,
        "mid2begratio_tap": mid2begratio_tapping
    }
    clean_signals = {
        "rec_tapping_clean": rec_tapping_clean,
        "rec_test_final": rec_test_final,
        "rec_markers_final": rec_markers_final,
        "rec_markers_clean": rec_markers_clean,
        "ratio_clean": ratio_clean,
        "snr_info": snr_info
    }
    return clean_signals


def signal_cleaning_tapping(rec_tapping, clean_location_ratio, clean_normalize_factor):
    """
    Cleaning heuristic to apply to tapping signal

    :type rec_tapping: numpy.ndarray
    :param rec_tapping: Audio recording

    :type clean_location_ratio: int
    :param clean_location_ratio: Sampling frequency.

    :type clean_normalize_factor: float
    :param clean_normalize_factor: Compression exponent to extract the envelope from the recording.

    :return: Returns the cleaned tapping signal and the start/ end cleaning ratios
    """
    start_normalize_ratio = min(clean_location_ratio)
    end_normalize_ratio = max(clean_location_ratio)
    start_include = round(len(rec_tapping) * start_normalize_ratio)  # only normalize middle part of the stimulus 1/3
    end_include = round(len(rec_tapping) * end_normalize_ratio)

    rec_tapping_tonormalize = np.copy(rec_tapping)
    rec_tapping_tonormalize[0:start_include] = rec_tapping_tonormalize[0:start_include] * clean_normalize_factor
    rec_tapping_tonormalize[end_include:-1] = rec_tapping_tonormalize[end_include:-1] * clean_normalize_factor

    rec_tapping_clean = 1.0 * rec_tapping / max(np.abs(rec_tapping_tonormalize))
    rec_tapping_clean[np.abs(rec_tapping_clean) > 1] = 1
    return rec_tapping_clean, start_include, end_include


def prepare_onsets_extraction(markers_onsets, stim_onsets, onset_is_played):
    """
    Prepare stimulus information for the onset extraction procedure

    :type markers_onsets: numpy.ndarray
    :param markers_onsets: List of markers onsets.

    :type stim_onsets: numpy.ndarray
    :param stim_onsets: List of stimulus onsets.

    :type onset_is_played: numpy.ndarray
    :param onset_is_played: List of booleans indicating whether each onset is played or not.

    :return: Returns the stimulus initial onsets ready for the onset extraction procedure (dict).
    """
    markers_onsets, stim_onsets_all_info = convert_to_numpy(markers_onsets, stim_onsets, onset_is_played)
    initial_onsets = {
        'markers_onsets': markers_onsets,
        'stim_onsets_all_info': stim_onsets_all_info,
        'stim_onsets_played': [c[0] for c in stim_onsets_all_info if (abs(c[1] - 1) < 1e-10)],
        'stim_onsets': [c[0] for c in stim_onsets_all_info],
        'stim_onsets_unplayed': [c[0] for c in stim_onsets_all_info if (abs(c[1] - 0) < 1e-10)]
    }
    return initial_onsets


def convert_to_numpy(markers_onsets, stim_onsets, onset_is_played):
    stim_onsets_all_info = np.ones([len(stim_onsets), 2])
    stim_onsets_all_info[:, 0] = np.array(stim_onsets)
    stim_onsets_all_info[:, 1] = np.array(onset_is_played)
    markers_onsets = np.array(markers_onsets)
    return markers_onsets, stim_onsets_all_info


########################################
# Functions supporting onset alignment
########################################
def align_onsets(initial_onsets, raw_extracted_onsets, markers_matching_window, onset_matching_window,
                 onset_matching_window_phase):
    """
    Align tapping and stimulus onsets, as well as detected and known markers onsets

    :type initial_onsets: dict
    :param initial_onsets: list of initial onsets including stimulus and markers

    :type raw_extracted_onsets: dict
    :param raw_extracted_onsets: list of extracted onsets including tapping and markers

    :type markers_matching_window: float
    :param markers_matching_window: Matching window to detect markers onsets corresponding to the known markers onsets.

    :type markers_matching_window: float
    :param markers_matching_window: Matching window to detect tapping onsets corresponding to the stimulus onsets.

    :type onset_matching_window: float
    :param onset_matching_window:  Matching window to detect tapping onsets corresponding to the stimulus onsets (ms).

    :type onset_matching_window_phase: float
    :param onset_matching_window_phase: Matching window to detect tapping onsets corresponding to the stimulus onsets.

    :return: Returns re-aligned onsets with key information (dict).
    """
    # get extracted onsets
    markers_detected_onsets = raw_extracted_onsets['markers_detected_onsets']
    tapping_detected_onsets = raw_extracted_onsets['tapping_detected_onsets']

    # use marker onsets to define a window for cleaning tapping onsets near markers
    markers_onsets = initial_onsets['markers_onsets']
    min_marker_isi = np.min(np.diff(np.array(markers_onsets)))

    # get initial onsets:
    stim_onsets = initial_onsets['stim_onsets']
    stim_onsets_played = initial_onsets['stim_onsets_played']
    # correct onsets start point using the first markers as the single frame of reference
    stim_onsets_corrected = stim_onsets - markers_onsets[0]
    stim_onsets_played_corrected = stim_onsets_played - markers_onsets[0]
    stim_onsets_played_corrected = np.less(
        [min(np.abs(onset - stim_onsets_played_corrected)) for onset in stim_onsets_corrected], 1.0)
    tapping_onsets_corrected = tapping_detected_onsets - markers_detected_onsets[0]
    # ideal  version of markers
    markers_onsets_aligned = markers_onsets - markers_onsets[0] + markers_detected_onsets[0]
    # find artifacts and remove
    markers_in_tapping = np.less([(min(np.abs(onset - markers_onsets_aligned))) for onset in tapping_detected_onsets],
                                 min_marker_isi)  # find markers in the tapping signal: Note changed from
    # onset_matching_window to something based on marker ISI
    onsets_before_after_markers = np.logical_or(np.less(tapping_detected_onsets, min(markers_onsets_aligned)),
                                                np.less(max(markers_onsets_aligned),
                                                        tapping_detected_onsets))  # find onsets before/ after markers
    is_tapping_ok = np.logical_and(~markers_in_tapping, ~onsets_before_after_markers)  # are the tapping onsets ok?
    tapping_onsets_corrected = tapping_onsets_corrected[
        is_tapping_ok]  # filter markers from tapping (using corrected version)
    tapping_detected_onsets = tapping_detected_onsets[is_tapping_ok]  # filter markers from tapping (using raw version)
    # align onsets
    matched_onsets = compute_matched_onsets(stim_onsets_corrected, tapping_onsets_corrected, onset_matching_window,
                                            onset_matching_window_phase)
    stim_detected_onsets = stim_onsets - markers_onsets[0] + markers_detected_onsets[0]
    stim_aligned = matched_onsets['stim_matched'] + stim_onsets[0] - markers_onsets[0] + markers_detected_onsets[0]
    resp_aligned = matched_onsets['resp_matched'] + stim_onsets[0] - markers_onsets[0] + markers_detected_onsets[0]
    # verify markers
    onsets_detection_info = verify_onsets_detection(markers_detected_onsets, markers_onsets, markers_matching_window,
                                                    onset_matching_window_phase)
    # print("stim onsets aligned = ")
    # print(stim_aligned)
    # print("tapping onsets aligned = ")
    # print(resp_aligned)
    print("response-stimulus asynchronies = ")
    print(matched_onsets['asynchrony'])
    aligned_onsets = {
        'stim_onsets_input': stim_onsets,
        'stim_onsets_detected': stim_detected_onsets,
        'resp_onsets_detected': tapping_detected_onsets,
        'stim_onsets_is_played': stim_onsets_played_corrected,
        'stim_onsets_aligned': stim_aligned,
        'resp_onsets_aligned': resp_aligned,
        'stim_ioi': matched_onsets['stim_ioi'],
        'resp_ioi': matched_onsets['resp_ioi'],
        'asynchrony': matched_onsets['asynchrony'],
        'mean_async': matched_onsets['mean_async'],
        'first_stim': matched_onsets['first_stim'],
        'num_resp_raw_onsets': float(np.size(tapping_detected_onsets)),
        'num_stim_raw_onsets': float(np.size(stim_onsets, 0)),
        'markers_onsets_detected': markers_detected_onsets,
        'markers_onsets_aligned': markers_onsets_aligned,
        'markers_onsets_input': markers_onsets,
        **onsets_detection_info  # info about markers detection procedure
    }
    return aligned_onsets


def compute_matched_onsets(stim_raw, resp_raw, max_proximity, max_proximity_phase):
    """
    Execute the matching onsets procedure

    :type stim_raw:  numpy.ndarray
    :param stim_raw: Stimulus onsets.

    :type resp_raw:  numpy.ndarray
    :param resp_raw: Response onsets.

    :type max_proximity: float
    :param max_proximity: Matching window to detect onsets.

    :type max_proximity_phase: float
    :param max_proximity_phase: Matching window to detect onsets in the matching phase.

    :return: Returns the re-aligned onsets with key information (dict).
    """
    if isinstance(stim_raw, list):
        stim_raw = np.array(stim_raw)
    if isinstance(resp_raw, list):
        resp_raw = np.array(resp_raw)
    mean_async = mean_asynchrony(stim_raw, resp_raw, max_proximity, max_proximity_phase)
    first_stim = stim_raw[0]
    resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony = raw_onsets_to_matched_onsets(
        stim_raw - first_stim,
        resp_raw - first_stim - mean_async,
        max_proximity,
        max_proximity_phase)
    resp = resp + mean_async  # correct for the mean asynchrony shift
    asynchrony = asynchrony + mean_async  # correct for the mean asynchrony shift
    matched_onsets = {'resp_matched': resp, 'stim_matched': stim, 'is_matched': is_matched,
                      'stim_ioi': stim_ioi, 'resp_ioi': resp_ioi, 'asynchrony': asynchrony,
                      'mean_async': mean_async, 'first_stim': first_stim
                      }
    return matched_onsets


def mean_asynchrony(stim_raw, resp_raw, max_proximity,
                    max_proximity_phase):  # compute mean asynchrony between stimulus and response
    """
    Calculate mean of asynchrony

    :type stim_raw:  numpy.ndarray
    :param stim_raw: Stimulus onsets.

    :type resp_raw:  numpy.ndarray
    :param resp_raw: Response onsets.

    :type max_proximity: float
    :param max_proximity: Matching window to detect onsets.

    :type max_proximity_phase: float
    :param max_proximity_phase: Matching window to detect onsets in the matching phase.

    :return: Returns the mean asynchrony
    """
    first_stim = stim_raw[0]
    _, _, _, _, _, asynchrony = raw_onsets_to_matched_onsets(
        stim_raw=stim_raw - first_stim,  # shift stimulus to start on ms-since-first-stim (i.e. 0)
        resp_raw=resp_raw - first_stim,
        max_proximity=max_proximity,
        max_proximity_phase=max_proximity_phase)
    if np.sum(~np.isnan(asynchrony)) > 0:
        return np.mean(asynchrony[~np.isnan(asynchrony)])
    else:
        return 0


def raw_onsets_to_matched_onsets(stim_raw, resp_raw, max_proximity, max_proximity_phase):
    """
    Match stim-response onsets from raw extracted onsets

    :type stim_raw:  numpy.ndarray
    :param stim_raw: Stimulus onsets.

    :type resp_raw:  numpy.ndarray
    :param resp_raw: Response onsets.

    :type max_proximity: float
    :param max_proximity: Matching window to detect onsets.

    :type max_proximity_phase: float
    :param max_proximity_phase: Matching window to detect onsets in the matching phase.

    :return: Returns matched onsets, asynchrony, and ioi
    """
    N = len(stim_raw)
    M = len(resp_raw)
    stim = np.full(N, np.nan)
    resp = np.full(N, np.nan)
    is_matched = np.full(N, np.nan)
    stim_ioi = np.full(N, np.nan)
    resp_ioi = np.full(N, np.nan)
    asynchrony = np.full(N, np.nan)

    stim_raw_used = np.full(N, np.nan)
    resp_raw_used = np.full(M, np.nan)

    if len(max_proximity_phase) == 0:  # default value if received empty value- this means this is not activated
        max_proximity_phase = [-1, 1]

    if len(resp_raw) == 0 or len(stim_raw) == 0:
        return resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony

    all_distances = []

    for j, _ in enumerate(stim_raw):

        # compute ISI before and after
        if j == 0:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_next

        elif (j + 1) == len(stim_raw):
            stim_last = stim_raw[j] - stim_raw[j - 1]
            stim_next = stim_last
        else:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_raw[j] - stim_raw[j - 1]

        for k, _ in enumerate(resp_raw):
            stim_proposal = stim_raw[j]
            resp_proposal = resp_raw[k]

            if resp_proposal > stim_proposal:
                phase = (resp_proposal - stim_proposal) / stim_next
            else:
                phase = (stim_proposal - resp_proposal) / stim_last
            distance_ms = abs(stim_proposal - resp_proposal)
            if (phase < max(max_proximity_phase)) and (phase > min(max_proximity_phase)) and (distance_ms < max_proximity):
                all_distances.append((phase, k, j))
    # print (stim_raw)
    # print (resp_raw)
    # scan all pairs find all unused pairs
    all_distances_nums = [phase for (phase, k, j) in all_distances]
    step = 0
    # print(all_distances)
    # perform greedy match pair the most proximal pair
    while len(all_distances_nums) > 0:
        # print(len(all_distances_nums))
        # print(all_distances_nums)
        # find the most proximal pair and match them
        best_score = min(all_distances_nums)
        best_kj_list = [(k, j) for (phase, k, j) in all_distances if (phase == best_score) and ((np.isnan(stim_raw_used[j])) and (np.isnan(resp_raw_used[k])))]
        k = best_kj_list[0][0]
        j = best_kj_list[0][1]

        is_matched[j] = 0  # stim-resp are matched
        stim[j] = stim_raw[j]
        resp[j] = resp_raw[k]
        stim_raw_used[j] = step
        resp_raw_used[k] = step

        all_distances_nums = [phase for (phase, k, j) in all_distances if
                              (np.isnan(stim_raw_used[j])) and (np.isnan(resp_raw_used[k]))]
        step = step + 1

    for j in range(1, N):
        stim_ioi[j] = stim[j] - stim[j - 1]
        resp_ioi[j] = resp[j] - resp[j - 1]
        asynchrony[j] = resp[j] - stim[j]
    asynchrony[0] = resp[0] - stim[0]  # first onset was not included before
    return resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony


# without greedy alignment but with phase
def raw_onsets_to_matched_onsets_old(stim_raw, resp_raw, max_proximity, max_proximity_phase):
    """
    Match stim-response onsets from raw extracted onsets

    :type stim_raw:  numpy.ndarray
    :param stim_raw: Stimulus onsets.

    :type resp_raw:  numpy.ndarray
    :param resp_raw: Response onsets.

    :type max_proximity: float
    :param max_proximity: Matching window to detect onsets.

    :type max_proximity_phase: float
    :param max_proximity_phase: Matching window to detect onsets for phase window.

    :return: Returns matched onsets, asynchrony, and ioi
    """
    N = len(stim_raw)
    stim = np.full(N, np.nan)
    resp = np.full(N, np.nan)
    is_matched = np.full(N, np.nan)
    stim_ioi = np.full(N, np.nan)
    resp_ioi = np.full(N, np.nan)
    asynchrony = np.full(N, np.nan)
    position = 0

    if len(max_proximity_phase) == 0:  # default value if received empty value- this means this is not activated
        max_proximity_phase = [-1, 1]

    if len(resp_raw) == 0 or len(stim_raw) == 0:
        return resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony
    for j, _ in enumerate(stim_raw):

        # compute ISI before and after
        if j == 0:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_next

        elif (j + 1) == len(stim_raw):
            stim_last = stim_raw[j] - stim_raw[j - 1]
            stim_next = stim_last
        else:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_raw[j] - stim_raw[j - 1]

        # print ("j= {} stim_last: {} stim_next:{}  ".format(j,stim_last,stim_next))
        # compute candidates within the window
        candidates = []
        for k, _ in enumerate(resp_raw):
            stim_proposal = stim_raw[j]
            resp_proposal = resp_raw[k]

            if resp_proposal > stim_proposal:
                phase = (resp_proposal - stim_proposal) / stim_next
            else:
                phase = (stim_proposal - resp_proposal) / stim_last
            if (phase < max(max_proximity_phase)) and (phase > min(max_proximity_phase)):
                candidates.append((phase, k))
        # print (candidates)
        # find the best match
        if len(candidates) == 0:
            did_not_find = True
        else:
            best_score = min([abs(phase) for (phase, k) in candidates])
            best_k_list = [k for (phase, k) in candidates if phase == best_score]
            best_k = best_k_list[0]
            did_not_find = False
            # print ("best_k== {} best_score: {} did_not_find:{}  ".format(best_k,best_score,did_not_find))
            # print ("resp_raw[best_k]= {}  ".format(resp_raw[best_k]))

        # print ("stim_raw[j]= {}  ".format(stim_raw[j]))

        if not did_not_find:
            resp_stim_min_diff = abs(resp_raw[best_k] - stim_raw[j])  # match resp-stim based on min diff
            position_min_diff = best_k

        # if phase is OK, now check using max_proximity.  If you don't want to do max_proximity you
        # can set its value to something big
        if not did_not_find and abs(resp_stim_min_diff) < max_proximity:
            is_matched[position] = 0  # stim-resp are matched
            stim[position] = stim_raw[j]
            resp[position] = resp_raw[position_min_diff]
            if j > 0:
                stim_ioi[position] = stim_raw[j] - stim_raw[j - 1]
            if position > 0:
                resp_ioi[position] = resp[position] - resp[position - 1]
            asynchrony[position] = resp[position] - stim[position]
        else:
            is_matched[position] = 1  # stim-resp are not matched
            stim[position] = stim_raw[j]
            resp[position] = np.nan
            if j > 0:
                stim_ioi[position] = stim_raw[j] - stim_raw[j - 1]
            if position > 0:
                resp_ioi[position] = np.nan
                resp_ioi[0] = np.nan
            asynchrony[position] = np.nan
        position += 1
    return resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony


# compatible with old scripts (no phase alignment)
def raw_onsets_to_matched_onsets_legacy(stim_raw, resp_raw, max_proximity):
    """ Match stimulus-response onsets from raw extracted onsets

    :type stim_raw:  numpy.ndarray
    :param stim_raw: Stimulus onsets.

    :type resp_raw:  numpy.ndarray
    :param resp_raw: Response onsets.

    :type max_proximity: float
    :param max_proximity: Matching window to detect onsets.

    :return: Returns matched onsets, asynchrony, and ioi
    """
    N = len(stim_raw)
    stim = np.full(N, np.nan)
    resp = np.full(N, np.nan)
    is_matched = np.full(N, np.nan)
    stim_ioi = np.full(N, np.nan)
    resp_ioi = np.full(N, np.nan)
    asynchrony = np.full(N, np.nan)
    position = 0
    if len(resp_raw) == 0 or len(stim_raw) == 0:
        return resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony
    for j, _ in enumerate(stim_raw):
        resp_stim_min_diff = min(abs(resp_raw - stim_raw[j]))  # match resp-stim based on min diff
        position_min_diff = np.argmin(abs(resp_raw - stim_raw[j]))  # find position
        if abs(resp_stim_min_diff) < max_proximity:
            is_matched[position] = 0  # stim-resp are matched
            stim[position] = stim_raw[j]
            resp[position] = resp_raw[position_min_diff]
            if j > 0:
                stim_ioi[position] = stim_raw[j] - stim_raw[j - 1]
            if position > 0:
                resp_ioi[position] = resp[position] - resp[position - 1]
            asynchrony[position] = resp[position] - stim[position]
        else:
            is_matched[position] = 1  # stim-resp are not matched
            stim[position] = stim_raw[j]
            resp[position] = np.nan
            if j > 0:
                stim_ioi[position] = stim_raw[j] - stim_raw[j - 1]
            if position > 0:
                resp_ioi[position] = np.nan
                resp_ioi[0] = np.nan
            asynchrony[position] = np.nan
        position += 1
    return resp, stim, is_matched, stim_ioi, resp_ioi, asynchrony


def verify_onsets_detection(onsets_detected, onsets_ideal, max_proximity, max_proximity_phase):
    if isinstance(onsets_detected, list):
        onsets_detected = np.array(onsets_detected)
    if isinstance(onsets_ideal, list):
        onsets_ideal = np.array(onsets_ideal)
    resp, _, _, stim_ioi, _, asynchrony = raw_onsets_to_matched_onsets(
        stim_raw=onsets_ideal - onsets_ideal[0],
        resp_raw=onsets_detected - onsets_detected[0],
        max_proximity=max_proximity, max_proximity_phase=max_proximity_phase)
    if np.sum(~np.isnan(asynchrony)) > 0:
        num_detected = np.sum(~np.isnan(asynchrony))
        num_missed = len(onsets_ideal) - num_detected
        max_difference = np.max(np.abs(asynchrony[~np.isnan(asynchrony)]))
    else:
        num_detected = 0
        num_missed = len(np.isnan(asynchrony))
        max_difference = -1
    onsets_ideal_shifted = onsets_ideal - onsets_ideal[0] + onsets_detected[0]
    resp_shifted = resp + onsets_detected[0]
    onsets_detection_info = {
        'verify_num_detected': num_detected, 'verify_num_missed': num_missed,
        'verify_max_difference': max_difference, 'verify_stim_ioi': stim_ioi,
        'verify_asynchrony': asynchrony, 'verify_stim_shifted': onsets_ideal_shifted,
        'verify_resp_shifted': resp_shifted}
    return onsets_detection_info
