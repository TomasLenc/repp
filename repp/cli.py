####################################################################################################
# File:     cli.py
# Purpose:  Command line interface for the REPP package.
#
# Author:   Manuel Anglada-Tort, Peter Harrison, Nori Jacoby
####################################################################################################
import click
from repp import __version__


@click.group()
@click.version_option(__version__, "--version", "-v", message="%(version)s")
def repp():
    pass
