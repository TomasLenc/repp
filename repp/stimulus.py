####################################################################################################
# File:     stimulus.py
# Purpose:  Class with methods for the stimulus preparation step
#
# Author:   Manuel Anglada-Tort, Peter Harrison, Nori Jacoby
####################################################################################################
from __future__ import division
from scipy.io import wavfile
import numpy as np
import math
import json
import os

from .utils import (
    band_pass_sharp,
    simple_resample,
    fade_in_out,
    JSONSerializer
)


class REPPStimulus:
    """ A class for the stimulus preparation step

    Attributes
    ----------

    stim_name : str
        Stimulus name
    config : class
        Configuration parameters for the experiment (see ``config.py``)
    """

    def __init__(
            self,
            stim_name,
            config
    ):
        self.stim_name = stim_name
        self.config = config

    def filter_and_add_markers(self, stim, stim_onsets, onset_is_played):
        """ Filter stimulus and add markers

        Parameters
        ----------

        stim : ndarray
            1D array containing the original audio stimulus
        stim_onsets : ndarray
            1D array containing the stimulus onsets
        onset_is_played : ndarray
            1D array of booleans indicating whether each onset is played or not

        Returns
        -------

        stim_prepared : ndarray
            1D array containing the prepared stimulus
        stim_info : dict
            A dictionary with the stimulus information
        """
        # spectral filtering
        filtered_stim = self.filter_stim(self.config.FS, stim, self.config.STIM_RANGE, self.config.STIM_AMPLITUDE)
        stim_prepared, stim_info = self.add_markers_and_shift_stim(
            filtered_stim,
            stim,
            stim_onsets,
            onset_is_played,
            self.config)  # add markers and shift stim accordingly
        stim_info["stim_name"] = self.stim_name
        return stim_prepared, stim_info

    def prepare_stim_from_files(self, input_dir):
        """ Prepare  stimulus from files

        Parameters
        ----------

        input_dir : str
            Path to input dir

        Returns
        -------

        stim_prepared : ndarray
            1D array containing the prepared stimulus
        stim_info : dict
            A dictionary with stimulus information
        filenames : dict
            A dictionary with filenames for analysis
        """
        filenames = self.make_filenames()
        stim, stim_onsets, onset_is_played = self.load_stimulus_from_files(
            self.config.FS,
            os.path.join(input_dir, filenames['audio_filename']),
            os.path.join(input_dir, filenames['onset_filename']))
        if len(stim.shape) == 2:
            stim = stim[:, 0]  # if stereo, convert to mono
        stim_prepared, stim_info = self.filter_and_add_markers(stim, stim_onsets, onset_is_played)
        return stim_prepared, stim_info, filenames

    def prepare_stim_from_onsets(self, stim_onsets):
        """ Prepare  stimulus from onsets

        Parameters
        ----------

        stim_onsets : list
            List of stimulus onsets

        Returns
        -------

        stim_prepared : ndarray
            1D array containing the prepared stimulus
        stim_onset : dict
            A dictionary with key stimulus information
        """
        filenames = self.make_filenames()
        click_sound = self.load_click(self.config)  # create metronome sound
        stim, onset_is_played = self.make_stim_from_onsets(self.config.FS, stim_onsets, click_sound)
        stim_prepared, stim_info = self.filter_and_add_markers(stim, stim_onsets, onset_is_played)
        return stim_prepared, stim_info, filenames

    def filter_stim(self, fs, stim, stim_range, stim_amplitude):  # spectral filtering
        """ Spectral filtering

        Parameters
        ----------

        fs : int
            Sampling frequency
        stim : ndarray
            1D array containing the stimulus onsets
        stim_range : list
            Stimulus frequency range (Hz): [min, max]
        stim_amplitude : list
            Target amplitude for the audio stimulus to play in the recording phase

        Returns
        -------

        filtered_stim : ndarray
            1D array containing the filtered stimulus
        """
        filtered_stim = band_pass_sharp(stim, fs, min(stim_range), max(stim_range))
        filtered_stim = stim_amplitude * (stim - filtered_stim) / max(stim - filtered_stim)
        return filtered_stim

    def add_markers_and_shift_stim(self, filtered_stim, stim, stim_onsets, onset_is_played, config):
        """ Add markers onsets at the beginning and end of the stimulus and shift stim and list of onsets accordingly

        Parameters
        ----------

        filtered_stim : ndarray
            1D array containing the filtered stimulus
        stim : ndarray
            1D array containing the stimulus
        stim_onsets : ndarray
            1D array containing the stimulus onsets
        onset_is_played : ndarray
            1D array of booleans indicating whether each onset is played or not
        config : class
            Configuration parameters for the experiment (see ``config.py``)

        Returns
        -------

        stim_prepared : ndarray
            1D array containing the prepared stimulus
        stim_onset : dict
            A dictionary with key stimulus information
        """
        markers_sound = self.make_markers_sound(
            config.FS,
            config.MARKERS_DURATION,
            config.MARKERS_ATTACK,
            config.MARKERS_RANGE,
            config.MARKERS_AMPLITUDE
        )
        markers_onsets, markers_channel = self.add_markers_sound(
            config.FS,
            stim,
            config.MARKERS_IOI,
            config.MARKERS_BEGINNING,
            config.MARKERS_END,
            config.STIM_BEGINNING,
            config.MARKERS_END_SLACK
        )
        stim_shifted_onsets = [(onset + config.STIM_BEGINNING) for onset in stim_onsets]  # shift all onsets
        # prepare stim by adding markers sound to markers onsets
        stim_prepared = self.put_clicks_in_audio(markers_channel, config.FS, markers_sound, markers_onsets)
        stim_start_samples = int(round(config.STIM_BEGINNING * config.FS / 1000.0))
        stim_prepared[stim_start_samples:(stim_start_samples + len(filtered_stim))] = stim_prepared[stim_start_samples:(
                stim_start_samples + len(filtered_stim))] + filtered_stim
        stim_duration = len(stim_prepared) / config.FS
        print("markers_onsets=")
        print(markers_onsets)
        stim_info = {
            'stim_duration': stim_duration,
            'stim_onsets': stim_onsets,
            'stim_shifted_onsets': stim_shifted_onsets,
            'onset_is_played': onset_is_played,
            'markers_onsets': markers_onsets
        }
        return stim_prepared, stim_info

    def make_markers_sound(self, fs, markers_duration, markers_attack, markers_range, markers_amplitude):
        """ Make markers sound: The markers sound is created from a list of sound ranges using two equally
        balanced sounds. One sound is a filtered white noise within these ranges. The other is a complex tone
        with pure tones centered in the middle of each range.

        Parameters
        ----------

        fs : int
            Sampling frequency
        markers_duration : float
            Duration of the marker sound (ms)
        markers_attack : float
            Attack of the marker sound
        markers_range : list
            Markers frequency range (Hz): [min, max]
        markers_amplitude : float
            Target amplitude of the marker sound

        Returns
        -------

        markers_sound : ndarray
            1D array containing the marker sound
        """
        random_sed = np.random.RandomState(1234567)  # for reproducibility this will always create the same sound
        num_samples = int(fs * markers_duration / 1000.0)
        samples = random_sed.normal(0, 1, size=num_samples)  # randomize a "local" seed
        samples = fade_in_out(samples, fs, markers_attack)
        samples_tone = np.zeros(np.size(samples))

        if len(markers_range) > 2:
            samples_filtered = samples * 0
            for i in range(0, len(markers_range), 2):
                temp = band_pass_sharp(samples, fs, markers_range[i], markers_range[i + 1])
                samples_filtered = samples_filtered + temp
                fmn = math.sqrt(markers_range[i] * markers_range[i + 1])  # mean in log scale
                temp2 = self.make_click_sound(fmn, markers_duration, fs, attack=markers_attack)  # stack pure tones
                samples_tone = samples_tone + temp2
        else:
            fmn = math.sqrt(min(markers_range) * max(markers_range))  # mean in log scale
            samples_filtered = band_pass_sharp(samples, fs, min(markers_range), max(markers_range))
            samples_tone = self.make_click_sound(fmn, markers_duration, fs, attack=markers_attack)
        samples_tone = samples_tone / np.max(samples_tone)  # max normalize
        samples_filtered = samples_filtered / np.max(samples_filtered)  # max normalize the filtered sound
        markers_sound = 0.5 * samples_tone + 0.5 * samples_filtered  # combine sounds with equal balance
        markers_sound = markers_amplitude * markers_sound / np.max(markers_sound)  # normalize to the correct amplitude
        markers_sound = fade_in_out(markers_sound, fs, markers_attack)  # add anohter fade in and out
        return markers_sound

    def add_markers_sound(self, fs, stim, markers_ioi, markers_beginning, markers_end, stim_beginning,
                          markers_end_slack):
        """ Add markers at the beginning and end of the stimulus by placing a marker sound in each marker onset

        Parameters
        ----------

        fs : int
            Sampling frequency
        stim : ndarray
            1D array containing the stimulus
        markers_ioi : list
            List of markers' iois
        markers_beginning : float
            Time of the markers relative to the beginning of the file (ms)
        markers_end : float
            Time of the markers relative to the end of the stimulus (ms)
        markers_end_slack : float
            Duration of additional slack (including the markers) after the end of the stimulus (ms)
        stim_beginning : float
            Time of the stimulus relative to the beginning of the file (ms)

        Returns
        -------

        markers_onsets : ndarray
            1D array containing the markers onsets
        markers_channel : ndarray
            1D array containing the markers channel with the total length of the recording
        """
        markers_onsets_seed = np.cumsum(np.array(markers_ioi))  # convert from IOI to onsets
        stim_duration = 1000.0 * len(stim) / fs  # duration stim (ms)
        markers_onsets = np.concatenate((markers_onsets_seed + markers_beginning,
                                         markers_onsets_seed + markers_end + stim_beginning + stim_duration))
        # this channel has the total length of the target stimulus to play in the recording phase
        markers_channel = np.zeros([len(stim) + round((markers_end_slack + stim_beginning) * fs / 1000.0)])
        return markers_onsets, markers_channel

    def put_clicks_in_audio(self, audio, fs, click_sound, stim_onsets):
        """ Add a sound for each onset in list

        Parameters
        ----------

        audio : ndarray
            1D array containing the whole length of the audio
        fs : int
            Sampling frequency
        click_sound : ndarray
            1D array containing sound of the click to add for each onset
        stim_onsets : ndarray
            1D array containing the stimulus onsets

        Returns
        -------

        audio : ndarray
            1D array containing the audio stimulus with the click sounds
        """
        for onset in stim_onsets:
            sample = int(round(1.0 * fs * onset / 1000.0))
            audio[sample:(sample + len(click_sound))] = audio[sample:(sample + len(click_sound))] + click_sound
        return audio

    def load_click(self, config):
        """ Load click sound, either from file or generated.

        Parameters
        ----------

        config : class
            Configuration parameters for the experiment (see ``config.py``)

        Returns
        -------

        click_sound : ndarray
            1D array containing the sound of the click
        """
        if config.USE_CLICK_FILENAME:
            click_sound = self.load_resample_file(
                config.FS,
                config.CLICK_FILENAME,
                renormalize=1
            )
        else:
            click_sound = self.make_click_sound(
                config.CLICK_FREQUENCY,
                config.CLICK_DURATION,
                config.FS,
                config.CLICK_ATTACK
            )
        return click_sound

    def make_onsets_from_ioi(self, stim_ioi):
        """ Make onsets from a from ioi

        Parameters
        ----------

        stim_ioi : ndarray
            1D array containing list of stimulus onsets


        Returns
        -------

        stim_onsets : ndarray
            1D array containing list of stimulus onsets
        """
        stim_onsets = np.cumsum(np.array(stim_ioi))
        return stim_onsets

    def make_stim_from_onsets(self, fs, stim_onsets, click_sound):
        """ Make stimulus from list of onsets

        Parameters
        ----------

        fs : int
            Sampling frequency
        stim_onsets : ndarray
            1D array containing list of stimulus onsets
        click_sound : ndarray
            1D array containing the sound of the click

        Returns
        -------

        stim : ndarray
            1D array containing the stimulus
        onset_is_played : ndarray
            1D array of booleans indicating whether each onset is played or not
        """
        stim = np.zeros([int(max(stim_onsets) * fs / 1000.0) + len(click_sound) + 1])
        stim = self.put_clicks_in_audio(stim, fs, click_sound, stim_onsets)
        onset_is_played = (np.array(stim_onsets) > -999)
        print("stim_onsets = ")
        print(stim_onsets)
        print("onset_is_played = ")
        print(onset_is_played)
        return stim, onset_is_played

    def load_resample_file(self, target_fs, audio_filename, renormalize=-1):
        source_fs, samples = wavfile.read(audio_filename)  # export sampling rate and audio data
        resampled = simple_resample(samples, source_fs, target_fs)
        if renormalize > 0:
            resampled = renormalize * resampled / max(np.abs(resampled))
        return resampled

    def make_click_sound(self, frequency, duration, fs, attack):
        num_samples = int(fs * duration / 1000.0)
        raw_sine = np.sin(2 * np.pi * np.arange(num_samples) * frequency / fs)
        click_sound = fade_in_out(raw_sine, fs, attack)
        return click_sound

    def load_stimulus_from_files(self, fs, audio_filename, onsets_filename):
        """ Load stimulus from files.

        Parameters
        ----------

        fs : int
            Sampling frequency
        audio_filename : str
            Path to audio filename
        onsets_filename : str
            Path to onset filename (must specify the time for each onset (ms) and whether it is played or not)

        Returns
        -------

        stim : ndarray
            1D array containing the stimulus
        stim_onsets : ndarray
            1D array containing the stimulus onsets
        onset_is_played : ndarray
            1D array of booleans indicating whether each onset is played or not
        """
        stim_onsets, onset_is_played = self.read_onsets_from_file(onsets_filename)
        stim = self.load_resample_file(fs, audio_filename)
        return stim, stim_onsets, onset_is_played

    def read_onsets_from_file(self, onsets_filename):
        stim_onsets = self.read_onset_txt_file(onsets_filename)
        onset_is_played = np.array(list(stim_onsets[:, 1] == 1))
        stim_onsets = list(stim_onsets[:, 0])
        print("stim_onsets = ")
        print(stim_onsets)
        print("onset_is_played = ")
        print(onset_is_played)
        return stim_onsets, onset_is_played

    def read_onset_txt_file(self, onsets_filename):
        f = open(onsets_filename, 'r')
        d = f.readlines()
        f.close()
        vec = []
        pos = 0
        for l in d:
            vec.append([])
            for a in l.split(','):
                vec[pos].append(float(a))
            pos = pos + 1
        onsets = np.array(vec)
        return onsets

    def make_filenames(self):
        """ Make filenames to be used in the analysis

        Returns
        -------

        filenames : dict
            A dictionary with filenames
        """
        stim_name = self.stim_name
        filenames = {
            'audio_filename': f"{stim_name}.wav",
            'onset_filename': f"{stim_name}.txt",
            'stim_info_file': f"{stim_name}.json",
            'title_plot': f"DEMO {stim_name}",
            'plot_filename': f"plot_{stim_name}.png",
            'recording_filename': f"rec_{stim_name}.wav"
        }
        return filenames

    @classmethod
    def to_wav(cls, samples, filename, fs):
        wavfile.write(filename, rate=fs, data=np.array(samples, dtype=np.float32))

    @classmethod
    def to_json(cls, info, filename):
        info_write = json.dumps(info, cls=JSONSerializer)
        with open(filename, "w") as file:
            file.write(info_write)
