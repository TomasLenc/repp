####################################################################################################
# File: analysis.py
# Purpose: Main functions for analysis step
# Author: Manuel Anglada-Tort, Peter Harrison, Nori Jacoby
####################################################################################################
import numpy as np
from matplotlib import pyplot as plt
import gc
from repp import signal_processing as sp


class REPPAnalysis:
    """ Methods for the signal processing and analysis steps

    Attributes
    ----------

    config : class
        Configuration parameters for the experiment (see ``config.py``)
    """

    def __init__(
            self,
            config
    ):
        self.config = config

    def do_only_stats(self, stim_info, recording_filename):
        """ Perform analysis for sms experiments and plot figures with main results

        Parameters
        ----------

        stim_info : dict
            A dictionary with stimulus information
        recording_filename : str
            Path to the recording

        Returns
        -------

        output : dict
            A dictionary with the output of the signal processing step
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output of the failing criteria
        """
        _, audio_signals, _, aligned_onsets = self.do_signal_processing(recording_filename, stim_info)
        print("Tapping analysis...")
        output, analysis, is_failed = self.do_stats(aligned_onsets, self.config)
        print("Analysing results...")
        return output, analysis, is_failed

    def do_analysis(self, stim_info, recording_filename, title_plot, output_plot, dpi=300):
        """ Perform analysis for sms experiments and plot figures with main results

        Parameters
        ----------

        stim_info : dict
            A dictionary with stimulus information
        recording_filename : str
            Path to the recording
        title_plot : str
            Title plot
        output_plot : str
            Path to the plot output
        dpi : int
            Resolution for main figure

        Returns
        -------

        output : dict
            A dictionary with the output of the signal processing step
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output of the failing criteria
        """
        _, audio_signals, _, aligned_onsets = self.do_signal_processing(recording_filename, stim_info)
        print("Tapping analysis...")
        output, analysis, is_failed = self.do_stats(aligned_onsets, self.config)
        print("Analysing results...")
        fig = self.do_plot(title_plot, audio_signals, aligned_onsets, analysis, is_failed, self.config)
        self.save_local(fig, output_plot, dpi)  # save local
        print("Plot saved")
        del fig
        gc.collect()
        return output, analysis, is_failed

    def do_signal_processing(self, recording_filename, stim_info):
        """ Do signal processing: onset extraction and alignment

        Parameters
        ----------

        stim_info : dict
            A dictionary with key stimulus information
        recording_filename : str
            Path to the recording

        Returns
        -------

        initial_onsets : dict
            A dictionary with the initial onsets prepared for the signal processing step
        audio_signals : dict
            A dictionary with the output of the signal extraction and cleaning procedure
        raw_extracted_onsets : dict
            A dictionary with the output of the onset extraction procedure
        aligned_onsets : dict
            A dictionary with the output of the onset alignment procedure
        """
        print("Preparing initial onsets...")
        initial_onsets = sp.prepare_onsets_extraction(
            stim_info['markers_onsets'],
            stim_info['stim_shifted_onsets'],
            stim_info['onset_is_played'])
        print("Extracting audio signals from mono recording...")
        audio_signals = sp.extract_audio_signals(recording_filename, self.config)
        print("Extracting raw onsets from  audio signals...")
        raw_extracted_onsets = sp.extract_onsets(audio_signals, self.config)
        print("Aligning onsets...")
        aligned_onsets = sp.align_onsets(
            initial_onsets,
            raw_extracted_onsets,
            self.config.MARKERS_MATCHING_WINDOW,
            self.config.ONSET_MATCHING_WINDOW_MS,
            self.config.ONSET_MATCHING_WINDOW_PHASE)
        return initial_onsets, audio_signals, raw_extracted_onsets, aligned_onsets

    def do_stats(self, onsets_aligned, config):
        """ calculate main stats for sms experiments

        Parameters
        ----------

        onsets_aligned : dict
            A dictionary with the output of the onset alignment procedure
        config : class
            Configuration parameters for the experiment (see ``config.py``)

        Returns
        -------

        output : dict
            A dictionary with the output of the signal processing step
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output of the failing criteria
        """
        output = self.reformat_output(onsets_aligned)  # return in good format trial output
        asynchronies = onsets_aligned['asynchrony']
        mean_all, sd_all, number_of_resp_all, number_of_stim_all = self.compute_mean_std_async(
            asynchronies,
            config.MIN_NUM_ASYNC)  # at least 2 asynchronies to calculate a valid response
        mean_played, sd_played, number_of_resp_played, number_of_stim_played = self.compute_mean_std_async(
            asynchronies[onsets_aligned['stim_onsets_is_played']],
            config.MIN_NUM_ASYNC)
        mean_notplayed, sd_notplayed, number_of_resp_notplayed, number_of_stim_notplayed = self.compute_mean_std_async(
            asynchronies[~onsets_aligned['stim_onsets_is_played']],
            config.MIN_NUM_ASYNC)
        num_of_bad_taps = onsets_aligned['num_resp_raw_onsets'] - number_of_resp_all
        # check if markers are good to go
        if (abs(onsets_aligned['verify_max_difference']) < config.MARKERS_MAX_ERROR) and (
                onsets_aligned['verify_num_missed'] == 0):
            markers_status = "Good"
            markers_ok = True
        else:
            markers_status = "Bad"
            markers_ok = False
        analysis = {
            # markers performance
            'num_markers_onsets': len(onsets_aligned['markers_onsets_input']),
            'num_markers_detected': onsets_aligned['verify_num_detected'],
            'num_markers_missed': onsets_aligned['verify_num_missed'],
            'markers_max_difference': onsets_aligned['verify_max_difference'],
            'markers_status': markers_status,
            'markers_ok': markers_ok,
            # Results computed in all onsets
            'num_stim_raw_all': onsets_aligned['num_stim_raw_onsets'],
            'num_stim_aligned_all': number_of_stim_all,
            'num_resp_raw_all': onsets_aligned['num_resp_raw_onsets'],
            'num_resp_aligned_all': number_of_resp_all,
            'mean_async_all': mean_all,
            'sd_async_all': sd_all,
            'ratio_resp_to_stim': 100.0 * float(np.size(onsets_aligned['resp_onsets_detected'])) / float(
                np.size(onsets_aligned['stim_onsets_input'], 0)),  # ratio of raw onsets
            'percent_resp_aligned_all': 100.0 * number_of_resp_all / (0.0001 + 1.0 * number_of_stim_all),
            # percentage of aligned onsets
            'num_of_bad_taps': num_of_bad_taps,
            'percent_of_bad_taps_all': round(
                100.0 * (0.0001 + num_of_bad_taps) / (0.0001 + 1.0 * onsets_aligned['num_resp_raw_onsets'])),
            # this can be more than 100 percent
            # Results only in played onsets
            'num_resp_aligned_played': number_of_resp_played,
            'num_stim_aligned_played': number_of_stim_played,
            'mean_async_played': mean_played,
            'sd_async_played': sd_played,
            'percent_response_aligned_played': 100.0 * number_of_resp_played / (0.0001 + 1.0 * number_of_stim_played),
            # Results only in played onsets
            'num_resp_aligned_notplayed': number_of_resp_notplayed,
            'num_stim_aligned_notplayed': number_of_stim_notplayed,
            'mean_async_notplayed': mean_notplayed,
            'sd_async_notplayed': sd_notplayed,
            'percent_response_aligned_notplayed': 100.0 * number_of_resp_notplayed / (
                        0.0001 + 1.0 * number_of_stim_notplayed)
        }
        is_failed = self.failing_criteria(analysis, config)
        return output, analysis, is_failed

    def do_analysis_tapping_only(self, recording_filename, title_plot, output_plot, dpi=300):
        """ Perform analysis for unconstrained tapping experiments and plot figures with main results

        Parameters
        ----------

        recording_filename : str
            Path to the recording
        title_plot : str
            Title plot
        output_plot : str
            Path to the plot output
        dpi : int
            Resolution for main figure

        Returns
        -------

        output : dict
            A dictionary with the output of the signal processing step
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output of the failing criteria
        """
        audio_signals = sp.extract_audio_signals_tapping_only(recording_filename, self.config)
        print("Tapping analysis...")
        extracted_onsets = sp.extract_onsets_only_tapping(audio_signals, self.config)
        analysis = self.do_stats_only_tapping(extracted_onsets)
        print("Analysing results...")
        fig = self.do_plot_tapping_only(title_plot, audio_signals, extracted_onsets, analysis)
        self.save_local(fig, output_plot, dpi)  # save local
        print("Plot saved")
        del fig
        gc.collect()
        return audio_signals, extracted_onsets, analysis

    def save_local(self, fig, output_filename, dpi):
        if output_filename == '':
            fig.show()
        else:
            fig.savefig(
                output_filename, format="png",
                dpi=dpi,
                facecolor='w', edgecolor='w')
            fig.clf()

    def do_stats_only_tapping(self, extracted_onsets):
        """ calculate main stats for unconstrained tapping experiments

        Parameters
        ----------
        extracted_onsets : dict
            A dictionary with the output of the onset extraction step

        Returns
        ----------
        analysis : dict
            A dictionary with the output of the performance analysis
        """
        resp_onsets_detected = extracted_onsets['tapping_detected_onsets'].tolist()
        resp_ioi_detected = np.diff(extracted_onsets['tapping_detected_onsets']).tolist()
        num_resp_onsets_detected = extracted_onsets['num_tapping_detected_onsets']
        analysis = {
            'resp_onsets_detected': resp_onsets_detected,
            'resp_ioi_detected': resp_ioi_detected,
            'num_resp_onsets_detected': num_resp_onsets_detected,
            'median_ioi': np.nanmedian(resp_ioi_detected),
            'bpm': 60000 / np.nanmedian(resp_ioi_detected),
            'q1_ioi': np.nanpercentile(resp_ioi_detected, 25),
            'q3_ioi': np.nanpercentile(resp_ioi_detected, 75)
        }
        return analysis

    def reformat_output(self, onsets_aligned):
        output = {
            # stim
            'stim_onsets_input': np.round(onsets_aligned['stim_onsets_input'], 2).tolist(),
            'stim_onsets_detected': np.round(onsets_aligned['stim_onsets_detected'], 2).tolist(),
            'stim_onsets_aligned': np.round(onsets_aligned['stim_onsets_aligned'], 2).tolist(),
            'stim_ioi': np.round(onsets_aligned['stim_ioi'], 2).tolist(),
            # response
            'resp_onsets_detected': np.round(onsets_aligned['resp_onsets_detected'], 2).tolist(),
            'resp_onsets_aligned': np.round(onsets_aligned['resp_onsets_aligned'], 2).tolist(),
            'resp_ioi': np.round(onsets_aligned['resp_ioi'], 2).tolist(),
            # asyncrhony
            'resp_stim_asynch': np.round(onsets_aligned['asynchrony'], 2).tolist(),
            # markers
            'markers_onsets_input': np.round(onsets_aligned['markers_onsets_input'], 2).tolist(),
            'markers_onsets_detected': np.round(onsets_aligned['markers_onsets_detected'], 2).tolist(),
            'markers_onsets_aligned': np.round(onsets_aligned['markers_onsets_aligned'], 2).tolist(),
            'first_marker_detected': np.round(onsets_aligned['markers_onsets_detected'], 2)[0]
        }
        return output

    def compute_mean_std_async(self, asynchronies, min_num_async):
        """ Calculate mean and SD of asynchrony

        Parameters
        ----------

        asynchronies : list
            List of resp-stim asynchronies
        min_num_async : float
            Minimum possible number of asynchronies to run the function

        Returns
        -------

        mean_all : float
            Mean asynchrony
        sd_all : float
            SD of asynchrony
        number_of_resp : int
            Number of response onsets after alignment
        number_of_stim : int
            Number of stimulus onsets
        """
        if np.sum(~np.isnan(asynchronies)) >= min_num_async:
            mean_all = np.mean(asynchronies[~np.isnan(asynchronies)])
            sd_all = np.std(asynchronies[~np.isnan(asynchronies)])
        else:
            mean_all = 999
            sd_all = 999
        number_of_asynchronies = np.sum(~np.isnan(asynchronies))
        mean_all = float(mean_all)
        sd_all = float(sd_all)
        number_of_resp = int(number_of_asynchronies)
        number_of_stim = int(len(asynchronies))
        return mean_all, sd_all, number_of_resp, number_of_stim

    def failing_criteria(self, analysis, config):
        """ Perform the failing criteria determined by the config parameters

        Parameters
        ----------

        analysis : dict
            A dictionary with the output of the performance analysis
        config : class
            Configuration parameters for the experiment (see ``config.py``)

        Returns
        --------

        is_failed : dict
            A dictionary with the output of the failing criteria
        """
        all_markers_are_detected = analysis['num_markers_onsets'] == analysis['num_markers_detected']
        markers_error_is_low = analysis['markers_max_difference'] < config.MARKERS_MAX_ERROR
        min_num_taps_is_ok = analysis['ratio_resp_to_stim'] >= config.MIN_RAW_TAPS
        max_num_taps_is_ok = analysis['ratio_resp_to_stim'] <= config.MAX_RAW_TAPS
        tapping_async_is_ok = analysis['mean_async_all'] != 999 and analysis['sd_async_all'] > config.MIN_SD_ASYNC
        failed = not (
                    all_markers_are_detected and markers_error_is_low and min_num_taps_is_ok and max_num_taps_is_ok and tapping_async_is_ok)
        options = [all_markers_are_detected, markers_error_is_low, min_num_taps_is_ok, max_num_taps_is_ok,
                   tapping_async_is_ok]
        reasons = ["Not all markers detected", "Markers error too large", "Too few detected taps",
                   "Too many detected taps", "Error in asynchrony"]
        if False in options:
            index = options.index(False)
            reason = reasons[index]
        else:
            reason = "All good"
        is_failed = {'failed': failed, 'reason': reason}
        return is_failed

    def do_plot(self, title_plot, audio_signals, aligned_onsets, analysis, is_failed, config):
        """ Plot results

        Parameters
        ----------

        title_plot : str
            title of the plot
        audio_signals : dict
            A dictionary with the output of the signal extraction and cleaning procedure
        aligned_onsets : dict
            A dictionary with the output of the onset alignment procedure
        analysis : dict
            A dictionary with the output of the performance analysis
        is_failed : dict
            A dictionary with the output failing criteria
        config : class
            Configuration parameters for the experiment (see ``config.py``)

        Returns
        -------

        fig : class
            Figure with main results
        """
        tt = audio_signals['time_line_for_sample']
        mxlim = [min(tt), max(tt)]
        plt.clf()

        # first raw:
        plot_original_recording(title_plot, mxlim, tt, audio_signals, aligned_onsets, is_failed, 1, config)
        plot_tapping_rec(tt, mxlim, audio_signals, aligned_onsets, analysis, 2, config)
        plot_tapping_zoomed(tt, audio_signals, aligned_onsets, analysis, 3, config)

        # second raw:
        plot_markers_detection(mxlim, tt, audio_signals, aligned_onsets, analysis, 5, config)
        plot_markers_cleaned(mxlim, tt, audio_signals, aligned_onsets, 6, config)
        plot_markers_zoomed(tt, audio_signals, aligned_onsets, 7, config)
        plot_markers_error(mxlim, aligned_onsets, analysis, 8, config)

        # third raw:
        plot_asynchrony(mxlim, aligned_onsets, analysis, 9, config)
        plot_alignment(mxlim, aligned_onsets, 10, False, config)  # added plots to show alignment
        plot_alignment(mxlim, aligned_onsets, 11, True, config)  # added plots to show alignment (zoomed)
        plot_asynch_and_ioi(mxlim, aligned_onsets, analysis, 12, config)

        # other plots not used
        # plot_markers_cleaning(mxlim, tt, audio_signals, aligned_onsets, 10, config)
        # plot_markers_zoomed_cleaned(tt, audio_signals, aligned_onsets, 11, config)
        fig = plt.gcf()
        fig.set_size_inches(25, 10.5)
        return fig

    def do_plot_tapping_only(self, title_plot, audio_signals, raw_onsets_extracted, analysis):
        """ Plot results (unconstrained tapping)

        Parameters
        ----------

        title_plot : str
            title of the plot
        audio_signals : dict
            A dictionary with the output of the signal extraction and cleaning procedure
        raw_onsets_extracted : dict
            A dictionary with the output of the onset extraction procedure
        analysis : dict
            A dictionary with the output of the performance analysis

        Returns
        -------

        fig : class
            Figure with main results
        """

        print("plot tapping data (tapping only)...")
        tt = audio_signals['time_line_for_sample']
        rec_downsampled = audio_signals['rec_downsampled']
        R = raw_onsets_extracted['tapping_detected_onsets']
        R_clean = audio_signals['rec_tapping_clean']
        plt.clf()
        # plt.subplot(fig_subplots[0], fig_subplots[1], 1)
        plt.plot(tt, rec_downsampled)
        plt.plot(tt, R_clean)
        mmx = self.config.EXTRACT_THRESH[1]
        R = np.array(R)
        plt.plot(R / 1000.0, R * 0 + mmx, 'xr')
        plt.title('{}: {} detected taps; median(Q1-Q) = {:2.2f}({:2.2f}-{:2.2f})'.format(
            title_plot,
            raw_onsets_extracted['num_tapping_detected_onsets'],
            analysis['median_ioi'],
            analysis['q1_ioi'],
            analysis['q3_ioi']))
        plt.xlabel('Time (sec)')
        plt.ylabel('Amplitude')
        fig = plt.gcf()
        fig.set_size_inches(25, 10.5)
        return fig


##################################
# supporting functions for plots
##################################


def plot_original_recording(title_plot, mxlim, tt, audio_signals, aligned_onsets, is_failed, position_subplot,
                            config):  # plot the original recording
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_detected']
    plt.plot(tt, audio_signals['rec_downsampled'], 'k-')
    message = "Recording: {} \n Failed = {}; Reason = {}".format(
        title_plot,
        is_failed['failed'],
        is_failed['reason'])
    plt.title(message)
    y = np.max(audio_signals['rec_downsampled'])
    plt.plot(aligned_onsets['markers_onsets_detected'] / 1000,
             y * config.EXTRACT_THRESH[1] * np.ones(np.size(aligned_onsets['markers_onsets_detected'])), 'mx')
    plt.plot(aligned_onsets['markers_onsets_aligned'] / 1000,
             config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_aligned'])), 'go')
    if len(aligned_onsets['resp_onsets_detected']) > 0:
        plt.plot(Rraw / 1000.0,
                 np.max(audio_signals['rec_tapping_clean']) * config.EXTRACT_THRESH[1] * np.ones(np.size(Rraw)), 'xr')
    plt.plot(Sraw / 1000.0,
             np.max(audio_signals['rec_tapping_clean']) * config.EXTRACT_THRESH[0] * np.ones(np.size(Sraw)), 'og')
    plt.xlim(mxlim)
    # plt.xlabel('time (sec)')


def plot_alignment(mxlim, aligned_onsets, position_subplot, is_zoomed, config):  # plot the original recording
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_detected']
    stim_onsets_aligned = aligned_onsets['stim_onsets_aligned']
    resp_onsets_aligned = aligned_onsets['resp_onsets_aligned']
    num_aligned = np.sum(~np.isnan(resp_onsets_aligned))
    markers_onsets_detected = aligned_onsets['markers_onsets_detected']
    markers_onsets_aligned = aligned_onsets['markers_onsets_aligned']
    stim_raw = Sraw
    resp_raw = Rraw
    max_proximity_phase = config.ONSET_MATCHING_WINDOW_PHASE
    max_proximity = config.ONSET_MATCHING_WINDOW_MS
    if len(markers_onsets_aligned) > 0:
        plt.plot(np.array(markers_onsets_aligned) / 1000.0, 0.25 * np.ones(np.size(markers_onsets_aligned)), 'dm')
    if len(markers_onsets_detected) > 0:
        plt.plot(np.array(markers_onsets_detected) / 1000.0, 0.75 * np.ones(np.size(markers_onsets_detected)), 'bs')

    for j, _ in enumerate(stim_raw):
        # compute ISI before and after
        if j == 0:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_next

        elif (j + 1) == len(stim_raw):
            stim_last = stim_raw[j] - stim_raw[j - 1]
            stim_next = stim_last
        else:
            stim_next = stim_raw[j + 1] - stim_raw[j]
            stim_last = stim_raw[j] - stim_raw[j - 1]

        for k, _ in enumerate(resp_raw):
            stim_proposal = stim_raw[j]
            resp_proposal = resp_raw[k]

            if resp_proposal > stim_proposal:
                phase = (resp_proposal - stim_proposal) / stim_next
            else:
                phase = (stim_proposal - resp_proposal) / stim_last
            distance_ms = abs(stim_proposal - resp_proposal)
            if (distance_ms < max_proximity) and (phase < max(max_proximity_phase)) and (
                    phase > min(max_proximity_phase)):
                plt.plot([resp_proposal / 1000.0, stim_proposal / 1000.0], [0, 1], 'y--')
            if distance_ms < max_proximity:
                plt.plot(resp_proposal / 1000.0, 0, 'cx')
            if (phase < max(max_proximity_phase)) and (phase > min(max_proximity_phase)):
                plt.plot(resp_proposal / 1000.0, 0, 'bx')
    #             if (phase<max(max_proximity_phase) and phase>min(max_proximity_phase)):
    #                 plt.plot([resp_proposal/1000.0,stim_proposal/1000.0],[0,1],'c--')
    if is_zoomed:
        min_x, max_x = find_min_max(Rraw, aligned_onsets, config)
        plt.xlim([min_x, max_x])
        plt.title("Zoomed view: Aligned onsets")
    else:
        plt.xlim(mxlim)
        plt.title("Aligned onsets Rraw={} Sraw={} aligned={}".format(len(Rraw), len(Sraw), num_aligned))
    if len(Rraw) > 0:
        plt.plot(np.array(Rraw) / 1000.0, 0 * np.ones(np.size(Rraw)), '+k')
    plt.plot(np.array(Sraw) / 1000.0, 1 * np.ones(np.size(Sraw)), 'og')
    for ll in range(len(resp_onsets_aligned)):
        plt.plot([resp_onsets_aligned[ll] / 1000.0, stim_onsets_aligned[ll] / 1000.0], [0, 1], 'k-')
        plt.plot(resp_onsets_aligned[ll] / 1000.0, 0, 'rx')


def plot_markers_detection(mxlim, tt, audio_signals, aligned_onsets, analysis, position_subplot, config):
    # plot markers detection (showing in red the test signal)
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    rec_markers_final = audio_signals['rec_markers_final']
    rec_test_final = audio_signals['rec_test_final']
    plt.plot(tt, rec_markers_final, 'b-')
    plt.plot(tt, rec_test_final, 'r-')
    percent_markers_detected = ((analysis["num_markers_detected"] / analysis["num_markers_onsets"]) * 100)
    message = 'Markers detection: \n {:2.0f}% markers detected ({} out of {})'.format(
        percent_markers_detected,
        analysis["num_markers_detected"],
        analysis["num_markers_onsets"],
        analysis["markers_max_difference"])
    plt.title(message)
    y = np.max(rec_markers_final)
    plot_markers(y, aligned_onsets, config)
    plt.xlim(mxlim)


def plot_markers_cleaning(mxlim, tt, audio_signals, aligned_onsets, position_subplot, config):
    # plot markers cleaning signal
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    snr_info = audio_signals['snr_info']
    plt.plot(tt, audio_signals['ratio_clean'], 'b-')
    message = "Cleaning markers signal: SNR: {:2.2f}".format(
        snr_info['snr'])
    plt.title(message)
    y = max(audio_signals['ratio_clean']) / 2
    plot_markers(y, aligned_onsets, config)
    plt.xlim(mxlim)


def plot_markers_cleaned(mxlim, tt, audio_signals, aligned_onsets, position_subplot, config):
    # plot cleaned marker signal
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    snr_info = audio_signals['snr_info']
    plt.plot(tt, audio_signals['rec_markers_clean'], 'b-')
    message = 'Markers cleaning signal: \n SNR: {:2.2f}; max_ratio: {:2.2f}; mean_ratio: {:2.2f}'.format(
        snr_info['snr'],
        snr_info['max_ratio'],
        snr_info['mean_ratio'])
    plt.title(message)
    y = max(audio_signals['rec_markers_clean'])
    plot_markers(y, aligned_onsets, config)
    plt.xlim(mxlim)


def plot_markers_zoomed(tt, audio_signals, aligned_onsets, position_subplot, config):
    # plot markers detection plot: zoomed
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    plt.plot(tt, audio_signals['rec_markers_final'], 'b-')
    plt.plot(tt, audio_signals['rec_test_final'], 'r-')
    message = 'Zoomed view on markers: (test signal in red)'
    plt.title(message)
    y = np.max(audio_signals['rec_markers_final'])
    plot_markers(y, aligned_onsets, config)
    mn1 = min([min(aligned_onsets['markers_onsets_detected']), min(aligned_onsets['markers_onsets_detected'])]) / 1000.0
    mx1 = mn1 + 0.8
    plt.xlim([mn1 - 0.1, mx1 + 0.1])


def plot_markers_zoomed_cleaned(tt, audio_signals, aligned_onsets, position_subplot, config):
    # plot markers detection plot: zoomed (cleaned signal)
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    plt.plot(tt, audio_signals['rec_markers_clean'], 'b-')
    message = 'Zoomed view on cleaned markers'
    plt.title(message)
    y = max(audio_signals['rec_markers_clean'])
    plot_markers(y, aligned_onsets, config)
    mn1 = min([min(aligned_onsets['markers_onsets_detected']), min(aligned_onsets['markers_onsets_detected'])]) / 1000.0
    mx1 = mn1 + 0.8
    plt.xlim([mn1 - 0.1, mx1 + 0.1])


def plot_markers_error(mxlim, aligned_onsets, analysis, position_subplot, config):
    # plot markers error
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    e_marker_ideal = aligned_onsets['verify_asynchrony']
    s_marker_ideal = aligned_onsets['verify_stim_ioi']
    S_marker_ideal = aligned_onsets['verify_stim_shifted']
    R_marker_ideal = aligned_onsets['verify_resp_shifted']
    plt.plot(S_marker_ideal / 1000.0, s_marker_ideal, 'sg')
    plt.plot(R_marker_ideal / 1000.0, s_marker_ideal + e_marker_ideal, 'rx')
    t = s_marker_ideal[~np.isnan(s_marker_ideal)]
    if len(aligned_onsets['markers_onsets_detected']) > 0 and len(t) > 0:
        plt.plot(aligned_onsets['markers_onsets_detected'] / 1000.0,
                 0.95 * np.min(t[t < 1000]) * np.ones(np.size(aligned_onsets['markers_onsets_detected'])), 'sm')
        plt.ylim([0.9 * min(t[t < 1000]), 1.05 * max(t[t < 1000])])
    if len(t) == 0:
        analysis["markers_max_difference"] = 9999
    message = 'Markers timing accuracy: ' + analysis['markers_status'] + '\n Error: {:2.2f} ms'.format(
        analysis["markers_max_difference"])
    plt.title('Legend inside')
    plt.title(message)
    plt.subplots_adjust(hspace=0.4, wspace=0.2)
    plt.xlim(mxlim)


def plot_tapping_rec(tt, mxlim, audio_signals, aligned_onsets, analysis, position_subplot, config):
    # plot tapping recording
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    rec_tap_final = audio_signals['rec_tapping_clean']
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_detected']
    plt.plot(tt, rec_tap_final, 'k-')
    message = 'Tapping detection: \n {:2.0f} of {:2.0f} stim onsets ({:2.2f}% ratio)\n {:2.2f}% bad taps'.format(
        analysis["num_resp_raw_all"],
        analysis["num_stim_raw_all"],
        analysis["ratio_resp_to_stim"],
        analysis["percent_of_bad_taps_all"])
    plt.title(message)
    if len(Rraw) > 0:
        plt.plot(Rraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[1] * np.ones(np.size(Rraw)), 'xr')
    plt.plot(Sraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[0] * np.ones(np.size(Sraw)), 'og')
    plt.plot(aligned_onsets['markers_onsets_aligned'] / 1000,
             config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_aligned'])), 'gs')
    plt.xlim(mxlim)


def plot_tapping_zoomed(tt, audio_signals, aligned_onsets, analysis, position_subplot, config):
    # plot tapping response: zoomed
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    rec_tap_final = audio_signals['rec_tapping_clean']
    Rraw = aligned_onsets['resp_onsets_detected']
    Sraw = aligned_onsets['stim_onsets_detected']
    plt.plot(tt, rec_tap_final, 'k-')
    message = 'Zoomed view on tapping: \n {} aligned resp onsets out of {} stim onsets ({:2.2f}%)'.format(
        analysis["num_resp_aligned_all"],
        analysis["num_stim_aligned_all"],
        analysis["percent_resp_aligned_all"])
    plt.title(message)
    if len(Rraw) > 0:
        plt.plot(Rraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[1] * np.ones(np.size(Rraw)), 'xr')
    plt.plot(Sraw / 1000.0, np.max(rec_tap_final) * config.EXTRACT_THRESH[0] * np.ones(np.size(Sraw)), 'og')
    plt.plot(aligned_onsets['markers_onsets_aligned'] / 1000,
             config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_aligned'])), 'gs')
    min_x, max_x = find_min_max(Rraw, aligned_onsets, config)
    plt.xlim([min_x, max_x])


def plot_asynchrony(mxlim, aligned_onsets, analysis, position_subplot, config):
    # plot tapping asynchrony
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    resp_onsets_aligned = aligned_onsets['resp_onsets_aligned']
    asynchrony = aligned_onsets['asynchrony']
    verify_asynchrony = aligned_onsets['verify_asynchrony']
    verify_markers_shifted = aligned_onsets['verify_stim_shifted']
    markers_onsets_aligned = aligned_onsets['markers_onsets_aligned']
    mean_all = analysis["mean_async_all"]
    std_all = analysis["sd_async_all"]

    plt.plot(verify_markers_shifted / 1000.0, verify_asynchrony, 'sg')
    plt.plot([min(markers_onsets_aligned) / 1000.0, max(markers_onsets_aligned) / 1000.0],
             [config.MARKERS_MATCHING_WINDOW, config.MARKERS_MATCHING_WINDOW], 'g--')
    plt.plot([min(markers_onsets_aligned) / 1000.0, max(markers_onsets_aligned) / 1000.0],
             [-config.MARKERS_MATCHING_WINDOW, -config.MARKERS_MATCHING_WINDOW], 'g--')
    if sum(~np.isnan(resp_onsets_aligned)) > 0:
        if mean_all < 999:
            plt.plot([min(markers_onsets_aligned) / 1000.0, max(markers_onsets_aligned) / 1000.0], [mean_all, mean_all],
                     'm-')
            if std_all > 0:
                plt.plot([min(markers_onsets_aligned) / 1000.0, max(markers_onsets_aligned) / 1000.0],
                         [mean_all + std_all, mean_all + std_all], 'y-')
                plt.plot([min(markers_onsets_aligned) / 1000.0, max(markers_onsets_aligned) / 1000.0],
                         [mean_all - std_all, mean_all - std_all], 'y-')
        if config.ONSET_MATCHING_WINDOW_MS < 500:
            plt.plot([min(resp_onsets_aligned[~np.isnan(resp_onsets_aligned)]) / 1000.0,
                      max(resp_onsets_aligned[~np.isnan(resp_onsets_aligned)]) / 1000.0],
                     [config.ONSET_MATCHING_WINDOW_MS, config.ONSET_MATCHING_WINDOW_MS], 'r--')
            plt.plot([min(resp_onsets_aligned[~np.isnan(resp_onsets_aligned)]) / 1000.0,
                      max(resp_onsets_aligned[~np.isnan(resp_onsets_aligned)]) / 1000.0],
                     [-config.ONSET_MATCHING_WINDOW_MS, -config.ONSET_MATCHING_WINDOW_MS], 'r--')
    plt.plot(resp_onsets_aligned / 1000.0, asynchrony, 'xr-')
    message = 'Tapping asynchrony: \n M = {:2.2f}ms; SD = {:2.2f}ms'.format(mean_all, std_all)
    plt.title(message)
    plt.xlim(mxlim)


def plot_asynch_and_ioi(mxlim, aligned_onsets, analysis, position_subplot, config):  # plot delay interval and ISI
    plt.subplot(config.PLOTS_TO_DISPLAY[0], config.PLOTS_TO_DISPLAY[1], position_subplot)
    stim_onsets_aligned = aligned_onsets['stim_onsets_aligned']
    stim_ioi = aligned_onsets['stim_ioi']
    is_played = aligned_onsets['stim_onsets_is_played']
    resp_onsets_aligned = aligned_onsets['resp_onsets_aligned']
    mean_async_played = analysis["mean_async_played"]
    sd_async_played = analysis["sd_async_played"]
    mean_async_notplayed = np.round(analysis["mean_async_notplayed"], 2)
    sd_async_notplayed = np.round(analysis["sd_async_notplayed"], 2)
    asynchrony = aligned_onsets['asynchrony']
    plt.plot(stim_onsets_aligned / 1000.0, stim_ioi, 'bs-')
    plt.plot(stim_onsets_aligned[~is_played] / 1000.0, stim_ioi[~is_played], 'cs-')
    plt.plot(resp_onsets_aligned / 1000.0, stim_ioi + asynchrony, 'xr-')
    if mean_async_notplayed == 999:
        mean_async_notplayed = "na"
        sd_async_notplayed = "na"
    message = 'Onset is played: {:2.0f}% tapping (M = {:2.2f}; SD = {:2.2f}) \n Onset is not played: {:2.0f}% tapping (M = {}; SD = {})'.format(
        analysis["percent_response_aligned_played"],
        mean_async_played,
        sd_async_played,
        analysis["percent_response_aligned_notplayed"],
        mean_async_notplayed,
        sd_async_notplayed)
    plt.title(message)
    plt.xlim(mxlim)


def plot_markers(y, aligned_onsets, config):
    plt.plot(aligned_onsets['markers_onsets_detected'] / 1000,
             y * config.EXTRACT_THRESH[1] * np.ones(np.size(aligned_onsets['markers_onsets_detected'])), 'mx')
    plt.plot(aligned_onsets['markers_onsets_detected'] / 1000,
             y * config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_detected'])), 'ms')
    plt.plot(aligned_onsets['markers_onsets_aligned'] / 1000,
             y * config.EXTRACT_THRESH[0] * np.ones(np.size(aligned_onsets['markers_onsets_aligned'])), 'go')


def find_min_max(Rraw, aligned_onsets, config):
    min_x = config.STIM_BEGINNING / 1000
    max_x = min_x + 5.0
    if len(Rraw) > 2:
        min_x = min(Rraw / 1000) - 0.5
        max_x = max(Rraw / 1000) + 0.5
    if len(Rraw) > 4:
        min_x = (Rraw[2] / 1000) - 0.5
        max_x = (Rraw[4] / 1000) + 0.5
    max_x = min(max_x, max(aligned_onsets['markers_onsets_detected'] / 1000))
    max_x = min(max_x, min_x + 10)
    return min_x, max_x
