####################################################################################################
# File:     utils.py
# Purpose:  Generic functions to support REPP
#
# Author:   Manuel Anglada-Tort, Peter Harrison, Nori Jacoby
####################################################################################################
from __future__ import division
from scipy.signal import buttord, butter, filtfilt, resample
from scipy.io import wavfile
import numpy as np
import json


class JSONSerializer(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, np.bool_):
            return super(JSONSerializer, self).encode(bool(obj))
        else:
            return super(JSONSerializer, self).default(obj)


def as_native_type(x):
    if type(x).__module__ == np.__name__:
        return x.item()
    return x


def save_samples_to_file(samples, filename, fs):
    wavfile.write(filename, rate=fs, data=np.array(samples, dtype=np.float32))


def save_json_to_file(stim_info, filename):
    stim_info_write = json.dumps(stim_info, cls=JSONSerializer)
    with open(filename, "w") as file:
        file.write(stim_info_write)


def read_audio_file(filename):
    print('reading audio file:')
    fs, samples = wavfile.read(filename)
    if samples.ndim == 2:
        print('input is stereo... summing down')
        samples = (samples[:, 0] + samples[:, 1]) / 2.0
    print('fs={} len(samples)={}'.format(fs, len(samples)))
    return samples, fs


def simple_resample(samples, source_fs, target_fs):
    if source_fs == target_fs:
        return samples
    else:
        resample_factor = float(target_fs) / float(source_fs)
        resampled = resample(samples, int(len(samples) * resample_factor))
    return resampled


def freq2nyq(freq, fs):
    # find the nyquist fq from fs
    # frequencies for filter design must be expressed as a fraction of Nyquist
    nyquist = fs / 2.0
    return freq / nyquist


def nyq2freq(nyq, fs):
    nyquist = fs / 2.0
    return nyq * nyquist


def ms_to_samples(ms, fs):
    # ms to sample conversion rounded to the next integer +1
    return int(np.floor(ms / 1000.0 * fs) + 1)


def samples_to_ms(sample, fs):
    return sample * 1000.0 / fs


def midi2freq(midi_number):
    # midi note number to frequency
    return (440.0 / 32.0) * (2 ** ((midi_number - 9) / 12.0))


def fade_in_out(rec, fs, attack_ms):
    fadein = np.power(np.linspace(0, 1, round(1 + (attack_ms / 1000.0 * fs))), 2.0)
    fadeout = fadein[::-1]
    rec[:len(fadein)] = rec[:len(fadein)] * fadein
    rec[-len(fadeout):] = rec[-len(fadeout):] * fadeout
    return rec


def band_pass_sharp(signal, fs, lower, upper):
    Wn1 = [freq2nyq(lower, fs), freq2nyq(upper, fs)]
    b1, a1 = build_filter_b(Wn1)
    filtered_signal1 = filtfilt(b1, a1, signal)
    filtered_signal2 = filtfilt(b1, a1, filtered_signal1)
    return filtered_signal2


def bandpass_geometry_freq(centroid, size=0.25):
    # bandpass filter around the centroid midi tone (centroid is supplies in midi note num!!)
    # bandpass size in semitones above and below center
    bandpass_lo = midi2freq(centroid - size)
    bandpass_hi = midi2freq(centroid + size)
    return bandpass_lo, bandpass_hi


def build_filter_a(wp, ws):
    # build a butterworth bandpass
    # computes W_n and best choice of N (order)
    N, Wn = buttord(wp, ws, 3, 9)
    # print("N (order) {}\nWn (1/nyq) {}\nWn (Hz) {}".format(N, Wn, list(map(nyq2freq,Wn))))
    b, a = butter(N, Wn, 'band', output='ba')
    return b, a


def build_filter_b(Wn):
    b, a = butter(2, Wn, btype='band', analog=False, output='ba')
    return b, a


def cheap_hilbert(samples, fs, window_length=2):
    """Cheap Hilbert replacer."""
    window_size = ms_to_samples(window_length, fs)
    result = np.abs(samples)
    for index, window_samples in enumerate(samples[0:(len(result) - window_size):window_size]):
        result[(index * window_size):(
                (index * window_size) + window_size)] = np.max(
            np.abs(samples[(index * window_size):((index * window_size) + window_size)]))
    return result
